﻿namespace Horizon.IT.Common.Web.Http.Web
{
    public interface IHttpService :IHttpPostService, IHttpGetService, IHttpPutService
    {
    }
}