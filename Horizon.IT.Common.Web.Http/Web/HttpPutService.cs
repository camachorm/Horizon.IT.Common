﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Threading.Tasks;
using Horizon.IT.Common.Abstractions.Interfaces;
using Horizon.IT.Common.Extensions;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace Horizon.IT.Common.Web.Http.Web
{
    [ExcludeFromCodeCoverage]
    public class HttpPutService : HttpPutPostBase, IHttpPutService
    {
        public HttpPutService() : this(new WebClient())
        {
        }

        public HttpPutService(WebClient client) : base(client)
        {
        }

        public async Task<JustHttpResponse<string>> PutJsonAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers, JObject messageData)
        {
            return await ExecuteAsync(logger, config, endpointUrl, new List<KeyValuePair<string, string>>
            {
                Constants.ContentTypes.ContentTypeApplicationJsonKeyValuePair
            }, messageData.ToString());
        }

        public async Task<JustHttpResponse<string>> PutJsonAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers, JToken messageData)
        {
            return await PutJsonAsync(logger, config, endpointUrl, new List<KeyValuePair<string, string>>(), messageData.ToJObject());
        }

        public async Task<JustHttpResponse<string>> PutStringAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl,
            IEnumerable<KeyValuePair<string, string>> headers, string messageData)
        {
            return await ExecuteAsync(logger, config, endpointUrl, headers, messageData);
        }
        
        public async Task<JustHttpResponse<byte[]>> PutDataAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl,
            IEnumerable<KeyValuePair<string, string>> headers, byte[] messageData)
        {
            return await ExecuteAsync(logger, config, endpointUrl, headers, messageData);
        }

        public async Task<JustHttpResponse<byte[]>> PutValuesAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl,
            IEnumerable<KeyValuePair<string, string>> headers, NameValueCollection messageData)
        {
            return await ExecuteAsync(logger, config, endpointUrl, headers, messageData);
        }

        protected override string Verb { get; } = "PUT";
    }
}
