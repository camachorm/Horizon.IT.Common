﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;
using Horizon.IT.Common.Abstractions.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace Horizon.IT.Common.Web.Http.Web
{

    public interface IHttpPostService
    {
        Task<JustHttpResponse<string>> PostJsonAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl,
            IEnumerable<KeyValuePair<string, string>> headers, JObject messageData);

        Task<JustHttpResponse<string>> PostJsonAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl,
            IEnumerable<KeyValuePair<string, string>> headers, JToken messageData);

        Task<JustHttpResponse<string>> PostStringAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl,
            IEnumerable<KeyValuePair<string, string>> headers, string messageData);

        Task<JustHttpResponse<byte[]>> PostDataAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl,
            IEnumerable<KeyValuePair<string, string>> headers, byte[] messageData);

        Task<JustHttpResponse<byte[]>> PostValuesAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl,
            IEnumerable<KeyValuePair<string, string>> headers, NameValueCollection messageData);
    }
}