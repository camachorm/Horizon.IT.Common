﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Horizon.IT.Common.Abstractions.Interfaces;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace Horizon.IT.Common.Web.Http.Web
{
    [ExcludeFromCodeCoverage]
    public class HttpService: IHttpService
    {
        private readonly IHttpPutService _putService;
        // ReSharper disable MemberCanBePrivate.Global
        public IHttpPostService PostService { get; }
        public IHttpGetService GetService { get; }
        // ReSharper restore MemberCanBePrivate.Global

        public HttpService(): this(new HttpPostService(), new HttpGetService(), new HttpPutService())
        {
        }

        public HttpService(IHttpPostService postService, IHttpGetService getService, IHttpPutService putService)
        {
            _putService = putService;
            PostService = postService;
            GetService = getService;
        }

        #region IHttpPostService Implementation
        public async Task<JustHttpResponse<string>> PostJsonAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers, JObject messageData)
        {
            return await PostService.PostJsonAsync(logger, config, endpointUrl, headers, messageData);
        }

        public async Task<JustHttpResponse<string>> PostJsonAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers, JToken messageData)
        {
            return await PostService.PostJsonAsync(logger, config, endpointUrl, headers, messageData);
        }

        public async Task<JustHttpResponse<string>> PostStringAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers, string messageData)
        {
            return await PostService.PostStringAsync(logger, config, endpointUrl, headers, messageData);
        }

        public async Task<JustHttpResponse<byte[]>> PostDataAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers, byte[] messageData)
        {
            return await PostService.PostDataAsync(logger, config, endpointUrl, headers, messageData);
        }

        public async Task<JustHttpResponse<byte[]>> PostValuesAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers,
            NameValueCollection messageData)
        {
            return await PostService.PostValuesAsync(logger, config, endpointUrl, headers, messageData);
        }

        #endregion

        #region IHttpGetService Implementation

        public async Task<JustHttpResponse<string>> GetStringAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers)
        {
            return await GetService.GetStringAsync(logger, config, endpointUrl, headers);
        }

        public async Task<JustHttpResponse<string>> GetStringAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers,
            IEnumerable<Tuple<string, string>> messageData)
        {
            return await GetService.ExecuteAsync(logger, config, endpointUrl, headers, messageData);
        }

        public async Task<JustHttpResponse<string>> GetStringAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers,
            Dictionary<string, string> messageData)
        {
            return await GetService.ExecuteAsync(logger, config, endpointUrl, headers, messageData);
        }

        public async Task<JustHttpResponse<string>> GetStringAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers,
            NameValueCollection messageData)
        {
            return await GetService.ExecuteAsync(logger, config, endpointUrl, headers, messageData);
        }

        public async Task<JustHttpResponse<string>> ExecuteAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers)
        {
            return await GetService.ExecuteAsync(logger, config, endpointUrl, headers);
        }

        public async Task<JustHttpResponse<string>> ExecuteAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers,
            NameValueCollection messageData)
        {
            return await GetService.ExecuteAsync(logger, config, endpointUrl, headers, messageData);
        }

        public async Task<JustHttpResponse<string>> ExecuteAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers,
            IEnumerable<Tuple<string, string>> messageData)
        {
            return await GetService.ExecuteAsync(logger, config, endpointUrl, headers, messageData);
        }

        public async Task<JustHttpResponse<string>> ExecuteAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers, Dictionary<string, string> messageData)
        {
            return await GetService.ExecuteAsync(logger, config, endpointUrl, headers, messageData);
        }

        #endregion

        #region IHttpPutService Implementation

        public async Task<JustHttpResponse<string>> PutJsonAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers,
            JObject messageData)
        {
            return await _putService.PutJsonAsync(logger, config, endpointUrl, headers, messageData);
        }

        public async Task<JustHttpResponse<string>> PutJsonAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers,
            JToken messageData)
        {
            return await _putService.PutJsonAsync(logger, config, endpointUrl, headers, messageData);
        }

        public async Task<JustHttpResponse<string>> PutStringAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers,
            string messageData)
        {
            return await _putService.PutStringAsync(logger, config, endpointUrl, headers, messageData);
        }

        public async Task<JustHttpResponse<byte[]>> PutDataAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers,
            byte[] messageData)
        {
            return await _putService.PutDataAsync(logger, config, endpointUrl, headers, messageData);
        }

        public async Task<JustHttpResponse<byte[]>> PutValuesAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers,
            NameValueCollection messageData)
        {
            return await _putService.PutValuesAsync(logger, config, endpointUrl, headers, messageData);
        } 

        #endregion
    }
}
