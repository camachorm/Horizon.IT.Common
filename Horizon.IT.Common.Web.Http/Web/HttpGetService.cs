﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Horizon.IT.Common.Abstractions.Interfaces;
using Horizon.IT.Common.Services;
using Microsoft.Extensions.Logging;

namespace Horizon.IT.Common.Web.Http.Web
{
    [ExcludeFromCodeCoverage]
    public class HttpGetService : HttpServiceBase, IHttpGetService
    {
        public HttpGetService() : this(new WebClient())
        {
        }

        public HttpGetService(WebClient client) : base(client)
        {
        }

        public async Task<JustHttpResponse<string>> ExecuteAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers, string messageData)
        {
            return await GenericRequest(logger, headers, endpointUrl, () => Client.DownloadString(endpointUrl));
        }

        public async Task<JustHttpResponse<byte[]>> ExecuteAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers, byte[] messageData)
        {
            return await GenericRequest(logger, headers, endpointUrl, () => Client.DownloadData(endpointUrl));
        }

        public async Task<JustHttpResponse<byte[]>> ExecuteAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers, NameValueCollection messageData)
        {
            return await GenericRequest(logger, headers, endpointUrl, () =>
            {
                foreach (string key in messageData.AllKeys)
                {
                    Client.QueryString.Add(key, messageData[key]);
                }
                return Client.DownloadData(endpointUrl);
            });
        }

        public async Task<JustHttpResponse<string>> GetStringAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers)
        {
            return await ExecuteAsync(logger, config, endpointUrl, headers, "");
        }

        public async Task<JustHttpResponse<string>> GetStringAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers,
                IEnumerable<Tuple<string, string>> queryParams)
        {
            return await ExecuteAsync(logger, config, endpointUrl, headers, queryParams);
        }

        public async Task<JustHttpResponse<string>> GetStringAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers,
                Dictionary<string, string> queryParams)
        {
            return await ExecuteAsync(logger, config, endpointUrl, headers, queryParams);
        }

        public async Task<JustHttpResponse<string>> GetStringAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers,
                NameValueCollection queryParams)
        {
            return await ((IService<JustHttpResponse<string>, Uri, IEnumerable<KeyValuePair<string, string>>, NameValueCollection>)this).ExecuteAsync(logger, config, endpointUrl, headers, queryParams);
        }

        public async Task<JustHttpResponse<string>> ExecuteAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers)
        {
            return await ExecuteAsync(logger, config, endpointUrl, headers, "");
        }

        public async Task<JustHttpResponse<string>> ExecuteAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers,
            IEnumerable<Tuple<string, string>> messageData)
        {
            return await GenericRequest(logger, headers, endpointUrl, () =>
            {
                var data = messageData.ToList();

                string baseQuery = string.Empty;
                if (string.IsNullOrEmpty(endpointUrl.Query))
                {
                    if (data.Any())
                    {
                        baseQuery = "?";
                    }
                }
                else
                {
                    baseQuery = endpointUrl.Query;
                    if (data.Any())
                    {
                        baseQuery += "&";
                    }
                }

                var query = data.Aggregate(baseQuery, (current, item) => current + $"{item.Item1}={item.Item2}&");

                if ((query?.EndsWith("&")).GetValueOrDefault())
                    query = query?.Substring(0, query.Length - 1);

                endpointUrl = new Uri(endpointUrl, query ?? "");


                logger?.LogInformation($"Performing Final transformed call to {endpointUrl.AbsoluteUri}, queryParams: {endpointUrl.Query}, headers: {Client.Headers.Count}");

                return Client.DownloadString(endpointUrl);
            });
        }

        public async Task<JustHttpResponse<string>> ExecuteAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers, Dictionary<string, string> messageData)
        {
            return await ExecuteAsync(logger, config, endpointUrl, headers,
                messageData.Select(i => new Tuple<string, string>(i.Key, i.Value)));
        }

        async Task<JustHttpResponse<string>> IService<JustHttpResponse<string>, Uri, IEnumerable<KeyValuePair<string, string>>, NameValueCollection>.ExecuteAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers,
                NameValueCollection messageData)
        {
            return await ExecuteAsync(logger, config, endpointUrl, headers,
                messageData.AllKeys.ToDictionary(key => key, key => messageData[key]));
        }
    }
}
