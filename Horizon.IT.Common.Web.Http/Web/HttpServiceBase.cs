﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Horizon.IT.Common.Abstractions.Configuration;
using Horizon.IT.Common.Exceptions;
using Horizon.IT.Common.Extensions;
using Microsoft.Extensions.Logging;
using Polly;

namespace Horizon.IT.Common.Web.Http.Web
{
    public abstract class HttpServiceBase : IHttpServiceBase
    {
        protected readonly WebClientWrapper Client;

        protected HttpServiceBase() : this(new WebClient())
        {
        }

        protected HttpServiceBase(WebClientWrapper client)
        {
            Client = client;
        }

        /// <summary>
        ///     Method used to perform a generic request that returns an object of type <see cref="TOut" />,
        ///     you can customize its behaviour by overriding <see cref="CarryOutRequest{TOut}" /> and injecting
        ///     <see cref="executionFunc" />
        /// </summary>
        /// <typeparam name="TOut">The expected return type of this operation</typeparam>
        /// <param name="logger">The <see cref="ILogger" /> instance to be used for logging.</param>
        /// <param name="headers">
        ///     The headers to be added to the request. (this will replace any headers within the
        ///     <see cref="WebClientWrapper" />
        /// </param>
        /// <param name="endpointUri">The URI to where we want to send the request.</param>
        /// <param name="executionFunc">
        ///     The delegate function to be executed to carry out the operation against the <see cref="WebClientWrapper" />.
        ///     e.g. - async () => await Client.DownloadStringAsync("messageData")
        /// </param>
        /// <returns>An instance of type <see cref="TOut" /></returns>
        [ExcludeFromCodeCoverage]
        public virtual async Task<JustHttpResponse<TOut>> GenericRequest<TOut>(ILogger logger,
            IEnumerable<KeyValuePair<string, string>> headers, Uri endpointUri, Func<TOut> executionFunc)
        {
            try
            {
                return await Policy.Handle<WebException>(ex => new[]
                    {
                        WebExceptionStatus.ConnectFailure,
                        WebExceptionStatus.ConnectionClosed,
                        WebExceptionStatus.Timeout,
                        WebExceptionStatus.RequestCanceled,
                        WebExceptionStatus.ConnectionClosed,
                        WebExceptionStatus.UnknownError
                    }.Contains(ex.Status))
                    .WaitAndRetryAsync(
                        MultipleConfiguration.GetSingleton()["Just.Common.Services.Web.Polly.MaxRetries"].ToSafeInt(3),
                        retryAttempt =>
                            TimeSpan.FromSeconds(
                                MultipleConfiguration.GetSingleton()["Just.Common.Services.Web.Polly.SleepBaseInterval"]
                                    .ToSafeInt(1) * retryAttempt))
                    .ExecuteAsync(() => Task.FromResult(CarryOutRequest(logger, headers, endpointUri, executionFunc)));
            }
            catch (WebException e)
            {
                var exception = new HorizonException("Http request failed irrecoverably", e);
                lock (Client)
                {
                    exception.AssociatedObjects.GetOrAdd("Response", e.Response);
                    exception.AssociatedObjects.GetOrAdd("ResponseHeaders", e.Response?.Headers);
                }

                throw exception;
            }
        }

        public virtual JustHttpResponse<TOut> CarryOutRequest<TOut>(ILogger logger,
            IEnumerable<KeyValuePair<string, string>> headers,
            Uri endpointUri, Func<TOut> executionFunc)
        {
            var response = new JustHttpResponse<TOut>();
            var headerList = headers.ToList();
            logger?.LogInformation(
                $"Performing call to {endpointUri.AbsoluteUri}, queryParams: {endpointUri.Query}, headers: {headerList.Count()}");
            lock (Client)
            {
                try
                {
                    Client.Headers.Clear();

                    Client.BaseAddress = endpointUri.AbsoluteUri;

                    foreach (var header in headerList)
                    {
                        logger?.LogDebug(
                            $"Adding header: {header.Key}/{!header.Value.IsNullOrEmpty()}/{header.Value.Length}");
                        Client.Headers.Add(header.Key, header.Value);
                    }

                    response.ResponseHeaders = Client.ResponseHeaders;
                    response.RequestUri = endpointUri;
                    response.ResponseValue = executionFunc.Invoke();
                }
                catch (Exception e)
                {
                    logger?.LogError(e, e.FullStackTrace());
                    throw;
                }
                finally
                {
                    try
                    {
                        var logEntries = "";
                        var keys = Client?.ResponseHeaders?.Keys.Cast<string>().ToList() ?? new List<string>();
                        foreach (var header in keys)
                            logEntries += $"\tHeader: {header} - {Client?.ResponseHeaders?[header]}\n";
                        logger?.LogInformation($"Listing Response Headers\n{logEntries}");
                    }
                    catch (Exception e)
                    {
                        logger?.LogDebug($"Error printing response headers in finally block of {GetType()}", e);
                    }
                }

                return response;
            }
        }
    }

    [ExcludeFromCodeCoverage]
    public class JustHttpResponse<T>
    {
        public WebHeaderCollection ResponseHeaders { get; set; }

        public Uri RequestUri { get; set; }

        public T ResponseValue { get; set; }
    }
}