﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Horizon.IT.Common.Abstractions.Interfaces;
using Microsoft.Extensions.Logging;

namespace Horizon.IT.Common.Web.Http.Web
{
    [ExcludeFromCodeCoverage]
    public abstract class HttpPutPostBase: HttpServiceBase
    {
        protected HttpPutPostBase(): base()
        {
        }

        protected HttpPutPostBase(WebClientWrapper client) : base(client)
        {
            UploadValues = (uri, messageData) =>
                Client.UploadValues(uri, Verb, messageData);

            UploadString = (uri, messageData) => Client.UploadString(uri, Verb, messageData);

            UploadData = (uri, messageData) => Client.UploadData(uri, Verb, messageData);
        }

        protected abstract string Verb { get; }

        protected readonly Func<Uri, NameValueCollection, byte[]> UploadValues;

        protected readonly Func<Uri, string, string> UploadString;
        
        protected readonly Func<Uri, byte[], byte[]> UploadData;


        #region IService Implementations 

        public async Task<JustHttpResponse<string>> ExecuteAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers, string messageData)
        {
            return await GenericRequest(logger, headers, endpointUrl, () => UploadString(endpointUrl, messageData));
        }

        public async Task<JustHttpResponse<byte[]>> ExecuteAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers, byte[] messageData)
        {
            return await GenericRequest<byte[]>(logger, headers, endpointUrl, () => UploadData(endpointUrl, messageData));
        }

        public async Task<JustHttpResponse<byte[]>> ExecuteAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers,
            NameValueCollection messageData)
        {
            return await GenericRequest<byte[]>(logger, headers, endpointUrl, () => UploadValues(endpointUrl, messageData));
        } 
        #endregion
    }
}
