﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Threading.Tasks;

namespace Horizon.IT.Common.Web.Http.Web
{
    public interface IWebClient
    {
        /// <summary>Gets or sets the base URI for requests made by a <see cref="T:System.Net.WebClient"></see>.</summary>
        /// <returns>A <see cref="T:System.String"></see> containing the base URI for requests made by a <see cref="T:System.Net.WebClient"></see> or <see cref="F:System.String.Empty"></see> if no base address has been specified.</returns>
        /// <exception cref="T:System.ArgumentException"><see cref="P:System.Net.WebClient.BaseAddress"></see> is set to an invalid URI. The inner exception may contain information that will help you locate the error.</exception>
         string BaseAddress { get; set; }
        /// <summary>Gets or sets the application's cache policy for any resources obtained by this WebClient instance using <see cref="T:System.Net.WebRequest"></see> objects.</summary>
        /// <returns>A <see cref="T:System.Net.Cache.RequestCachePolicy"></see> object that represents the application's caching requirements.</returns>
         RequestCachePolicy CachePolicy { get; set; }
        /// <summary>Gets or sets the network credentials that are sent to the host and used to authenticate the request.</summary>
        /// <returns>An <see cref="T:System.Net.ICredentials"></see> containing the authentication credentials for the request. The default is null.</returns>
         ICredentials Credentials { get; set; }
        /// <summary>Gets and sets the <see cref="T:System.Text.Encoding"></see> used to upload and download strings.</summary>
        /// <returns>A <see cref="T:System.Text.Encoding"></see> that is used to encode strings. The default value of this property is the encoding returned by <see cref="P:System.Text.Encoding.Default"></see>.</returns>
         Encoding Encoding { get; set; }
        /// <summary>Gets or sets a collection of header name/value pairs associated with the request.</summary>
        /// <returns>A <see cref="T:System.Net.WebHeaderCollection"></see> containing header name/value pairs associated with this request.</returns>
         WebHeaderCollection Headers { get; set; }
        /// <summary>Gets whether a Web request is in progress.</summary>
        /// <returns>true if the Web request is still in progress; otherwise false.</returns>
         bool IsBusy { get; }
        /// <summary>Gets or sets the proxy used by this <see cref="T:System.Net.WebClient"></see> object.</summary>
        /// <returns>An <see cref="T:System.Net.IWebProxy"></see> instance used to send requests.</returns>
        /// <exception cref="T:System.ArgumentNullException"><see cref="P:System.Net.WebClient.Proxy"></see> is set to null.</exception>
         IWebProxy Proxy { get; set; }
        /// <summary>Gets or sets a collection of query name/value pairs associated with the request.</summary>
        /// <returns>A <see cref="T:System.Collections.Specialized.NameValueCollection"></see> that contains query name/value pairs associated with the request. If no pairs are associated with the request, the value is an empty <see cref="T:System.Collections.Specialized.NameValueCollection"></see>.</returns>
         NameValueCollection QueryString { get; set; }
        /// <summary>Gets a collection of header name/value pairs associated with the response.</summary>
        /// <returns>A <see cref="T:System.Net.WebHeaderCollection"></see> containing header name/value pairs associated with the response, or null if no response has been received.</returns>
         WebHeaderCollection ResponseHeaders { get; }
        /// <summary>Gets or sets a <see cref="T:System.Boolean"></see> value that controls whether the <see cref="P:System.Net.CredentialCache.DefaultCredentials"></see> are sent with requests.</summary>
        /// <returns>true if the default credentials are used; otherwise false. The default value is false.</returns>
         bool UseDefaultCredentials { get; set; }
        /// <summary>Occurs when an asynchronous data download operation completes.</summary>
        /// <returns></returns>
         event DownloadDataCompletedEventHandler DownloadDataCompleted;
        /// <summary>Occurs when an asynchronous file download operation completes.</summary>
        /// <returns></returns>
         event AsyncCompletedEventHandler DownloadFileCompleted;
        /// <summary>Occurs when an asynchronous download operation successfully transfers some or all of the data.</summary>
        /// <returns></returns>
         event DownloadProgressChangedEventHandler DownloadProgressChanged;
        /// <summary>Occurs when an asynchronous resource-download operation completes.</summary>
        /// <returns></returns>
         event DownloadStringCompletedEventHandler DownloadStringCompleted;
        /// <summary>Occurs when an asynchronous operation to open a stream containing a resource completes.</summary>
        /// <returns></returns>
         event OpenReadCompletedEventHandler OpenReadCompleted;
        /// <summary>Occurs when an asynchronous operation to open a stream to write data to a resource completes.</summary>
        /// <returns></returns>
         event OpenWriteCompletedEventHandler OpenWriteCompleted;
        /// <summary>Occurs when an asynchronous data-upload operation completes.</summary>
        /// <returns></returns>
         event UploadDataCompletedEventHandler UploadDataCompleted;
        /// <summary>Occurs when an asynchronous file-upload operation completes.</summary>
        /// <returns></returns>
         event UploadFileCompletedEventHandler UploadFileCompleted;
        /// <summary>Occurs when an asynchronous upload operation successfully transfers some or all of the data.</summary>
        /// <returns></returns>
         event UploadProgressChangedEventHandler UploadProgressChanged;
        /// <summary>Occurs when an asynchronous string-upload operation completes.</summary>
        /// <returns></returns>
         event UploadStringCompletedEventHandler UploadStringCompleted;
        /// <summary>Occurs when an asynchronous upload of a name/value collection completes.</summary>
        /// <returns></returns>
         event UploadValuesCompletedEventHandler UploadValuesCompleted;
        /// <summary>Cancels a pending asynchronous operation.</summary>
         void CancelAsync();
        /// <summary>Downloads the resource as a <see cref="T:System.Byte"></see> array from the URI specified.</summary>
        /// <param name="address">The URI from which to download data.</param>
        /// <returns>A <see cref="T:System.Byte"></see> array containing the downloaded resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while downloading data.</exception>
        /// <exception cref="T:System.NotSupportedException">The method has been called simultaneously on multiple threads.</exception>
         byte[] DownloadData(string address);
        /// <summary>Downloads the resource as a <see cref="T:System.Byte"></see> array from the URI specified.</summary>
        /// <param name="address">The URI represented by the <see cref="T:System.Uri"></see> object, from which to download data.</param>
        /// <returns>A <see cref="T:System.Byte"></see> array containing the downloaded resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
         byte[] DownloadData(Uri address);
        /// <summary>Downloads the resource as a <see cref="T:System.Byte"></see> array from the URI specified as an asynchronous operation.</summary>
        /// <param name="address">A <see cref="T:System.Uri"></see> containing the URI to download.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while downloading the resource.</exception>
         void DownloadDataAsync(Uri address);
        /// <summary>Downloads the resource as a <see cref="T:System.Byte"></see> array from the URI specified as an asynchronous operation.</summary>
        /// <param name="address">A <see cref="T:System.Uri"></see> containing the URI to download.</param>
        /// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while downloading the resource.</exception>
         void DownloadDataAsync(Uri address, object userToken);
        /// <summary>Downloads the resource as a <see cref="T:System.Byte"></see> array from the URI specified as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to download.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.Byte"></see> array containing the downloaded resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while downloading the resource.</exception>
         Task<byte[]> DownloadDataTaskAsync(string address);
        /// <summary>Downloads the resource as a <see cref="T:System.Byte"></see> array from the URI specified as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to download.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.Byte"></see> array containing the downloaded resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while downloading the resource.</exception>
         Task<byte[]> DownloadDataTaskAsync(Uri address);
        /// <summary>Downloads the resource with the specified URI to a local file.</summary>
        /// <param name="address">The URI from which to download data.</param>
        /// <param name="fileName">The name of the local file that is to receive the data.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="filename">filename</paramref> is null or <see cref="F:System.String.Empty"></see>.   -or-   The file does not exist.   -or- An error occurred while downloading data.</exception>
        /// <exception cref="T:System.NotSupportedException">The method has been called simultaneously on multiple threads.</exception>
         void DownloadFile(string address, string fileName);
        /// <summary>Downloads the resource with the specified URI to a local file.</summary>
        /// <param name="address">The URI specified as a <see cref="T:System.String"></see>, from which to download data.</param>
        /// <param name="fileName">The name of the local file that is to receive the data.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="fileName">fileName</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="filename">filename</paramref> is null or <see cref="F:System.String.Empty"></see>.   -or-   The file does not exist.   -or-   An error occurred while downloading data.</exception>
        /// <exception cref="T:System.NotSupportedException">The method has been called simultaneously on multiple threads.</exception>
         void DownloadFile(Uri address, string fileName);
        /// <summary>Downloads, to a local file, the resource with the specified URI. This method does not block the calling thread.</summary>
        /// <param name="address">The URI of the resource to download.</param>
        /// <param name="fileName">The name of the file to be placed on the local computer.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="fileName">fileName</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while downloading the resource.</exception>
        /// <exception cref="T:System.InvalidOperationException">The local file specified by <paramref name="fileName">fileName</paramref> is in use by another thread.</exception>
         void DownloadFileAsync(Uri address, string fileName);
        /// <summary>Downloads, to a local file, the resource with the specified URI. This method does not block the calling thread.</summary>
        /// <param name="address">The URI of the resource to download.</param>
        /// <param name="fileName">The name of the file to be placed on the local computer.</param>
        /// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="fileName">fileName</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while downloading the resource.</exception>
        /// <exception cref="T:System.InvalidOperationException">The local file specified by <paramref name="fileName">fileName</paramref> is in use by another thread.</exception>
         void DownloadFileAsync(Uri address, string fileName, object userToken);
        /// <summary>Downloads the specified resource to a local file as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to download.</param>
        /// <param name="fileName">The name of the file to be placed on the local computer.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task"></see>.   The task object representing the asynchronous operation.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="fileName">fileName</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while downloading the resource.</exception>
        /// <exception cref="T:System.InvalidOperationException">The local file specified by <paramref name="fileName">fileName</paramref> is in use by another thread.</exception>
         Task DownloadFileTaskAsync(string address, string fileName);
        /// <summary>Downloads the specified resource to a local file as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to download.</param>
        /// <param name="fileName">The name of the file to be placed on the local computer.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task"></see>.   The task object representing the asynchronous operation.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="fileName">fileName</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while downloading the resource.</exception>
        /// <exception cref="T:System.InvalidOperationException">The local file specified by <paramref name="fileName">fileName</paramref> is in use by another thread.</exception>
         Task DownloadFileTaskAsync(Uri address, string fileName);
        /// <summary>Downloads the requested resource as a <see cref="T:System.String"></see>. The resource to download is specified as a <see cref="T:System.String"></see> containing the URI.</summary>
        /// <param name="address">A <see cref="T:System.String"></see> containing the URI to download.</param>
        /// <returns>A <see cref="T:System.String"></see> containing the requested resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while downloading the resource.</exception>
        /// <exception cref="T:System.NotSupportedException">The method has been called simultaneously on multiple threads.</exception>
         string DownloadString(string address);
        /// <summary>Downloads the requested resource as a <see cref="T:System.String"></see>. The resource to download is specified as a <see cref="T:System.Uri"></see>.</summary>
        /// <param name="address">A <see cref="T:System.Uri"></see> object containing the URI to download.</param>
        /// <returns>A <see cref="T:System.String"></see> containing the requested resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while downloading the resource.</exception>
        /// <exception cref="T:System.NotSupportedException">The method has been called simultaneously on multiple threads.</exception>
         string DownloadString(Uri address);
        /// <summary>Downloads the resource specified as a <see cref="T:System.Uri"></see>. This method does not block the calling thread.</summary>
        /// <param name="address">A <see cref="T:System.Uri"></see> containing the URI to download.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while downloading the resource.</exception>
         void DownloadStringAsync(Uri address);
        /// <summary>Downloads the specified string to the specified resource. This method does not block the calling thread.</summary>
        /// <param name="address">A <see cref="T:System.Uri"></see> containing the URI to download.</param>
        /// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while downloading the resource.</exception>
         void DownloadStringAsync(Uri address, object userToken);
        /// <summary>Downloads the resource as a <see cref="T:System.String"></see> from the URI specified as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to download.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.Byte"></see> array containing the downloaded resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while downloading the resource.</exception>
         Task<string> DownloadStringTaskAsync(string address);
        /// <summary>Downloads the resource as a <see cref="T:System.String"></see> from the URI specified as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to download.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.Byte"></see> array containing the downloaded resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while downloading the resource.</exception>
         Task<string> DownloadStringTaskAsync(Uri address);
        /// <summary>Returns a <see cref="T:System.Net.WebRequest"></see> object for the specified resource.</summary>
        /// <param name="address">A <see cref="T:System.Uri"></see> that identifies the resource to request.</param>
        /// <returns>A new <see cref="T:System.Net.WebRequest"></see> object for the specified resource.</returns>
        /// <summary>Opens a readable stream for the data downloaded from a resource with the URI specified as a <see cref="T:System.String"></see>.</summary>
        /// <param name="address">The URI specified as a <see cref="T:System.String"></see> from which to download data.</param>
        /// <returns>A <see cref="T:System.IO.Stream"></see> used to read data from a resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while downloading data.</exception>
         Stream OpenRead(string address);
        /// <summary>Opens a readable stream for the data downloaded from a resource with the URI specified as a <see cref="T:System.Uri"></see></summary>
        /// <param name="address">The URI specified as a <see cref="T:System.Uri"></see> from which to download data.</param>
        /// <returns>A <see cref="T:System.IO.Stream"></see> used to read data from a resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while downloading data.</exception>
         Stream OpenRead(Uri address);
        /// <summary>Opens a readable stream containing the specified resource. This method does not block the calling thread.</summary>
        /// <param name="address">The URI of the resource to retrieve.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and address is invalid.   -or-   An error occurred while downloading the resource.   -or-   An error occurred while opening the stream.</exception>
         void OpenReadAsync(Uri address);
        /// <summary>Opens a readable stream containing the specified resource. This method does not block the calling thread.</summary>
        /// <param name="address">The URI of the resource to retrieve.</param>
        /// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and address is invalid.   -or-   An error occurred while downloading the resource.   -or-   An error occurred while opening the stream.</exception>
         void OpenReadAsync(Uri address, object userToken);
        /// <summary>Opens a readable stream containing the specified resource as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to retrieve.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.IO.Stream"></see> used to read data from a resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and address is invalid.   -or-   An error occurred while downloading the resource.   -or-   An error occurred while opening the stream.</exception>
         Task<Stream> OpenReadTaskAsync(string address);
        /// <summary>Opens a readable stream containing the specified resource as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to retrieve.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.IO.Stream"></see> used to read data from a resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and address is invalid.   -or-   An error occurred while downloading the resource.   -or-   An error occurred while opening the stream.</exception>
         Task<Stream> OpenReadTaskAsync(Uri address);
        /// <summary>Opens a stream for writing data to the specified resource.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <returns>A <see cref="T:System.IO.Stream"></see> used to write data to the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while opening the stream.</exception>
         Stream OpenWrite(string address);
        /// <summary>Opens a stream for writing data to the specified resource, using the specified method.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <returns>A <see cref="T:System.IO.Stream"></see> used to write data to the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while opening the stream.</exception>
         Stream OpenWrite(string address, string method);
        /// <summary>Opens a stream for writing data to the specified resource.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <returns>A <see cref="T:System.IO.Stream"></see> used to write data to the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while opening the stream.</exception>
         Stream OpenWrite(Uri address);
        /// <summary>Opens a stream for writing data to the specified resource, by using the specified method.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <returns>A <see cref="T:System.IO.Stream"></see> used to write data to the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while opening the stream.</exception>
         Stream OpenWrite(Uri address, string method);
        /// <summary>Opens a stream for writing data to the specified resource. This method does not block the calling thread.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
         void OpenWriteAsync(Uri address);
        /// <summary>Opens a stream for writing data to the specified resource. This method does not block the calling thread.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
         void OpenWriteAsync(Uri address, string method);
        /// <summary>Opens a stream for writing data to the specified resource, using the specified method. This method does not block the calling thread.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while opening the stream.</exception>
         void OpenWriteAsync(Uri address, string method, object userToken);
        /// <summary>Opens a stream for writing data to the specified resource as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.IO.Stream"></see> used to write data to the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while opening the stream.</exception>
         Task<Stream> OpenWriteTaskAsync(string address);
        /// <summary>Opens a stream for writing data to the specified resource as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.IO.Stream"></see> used to write data to the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while opening the stream.</exception>
         Task<Stream> OpenWriteTaskAsync(string address, string method);
        /// <summary>Opens a stream for writing data to the specified resource as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.IO.Stream"></see> used to write data to the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while opening the stream.</exception>
         Task<Stream> OpenWriteTaskAsync(Uri address);
        /// <summary>Opens a stream for writing data to the specified resource as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.IO.Stream"></see> used to write data to the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while opening the stream.</exception>
         Task<Stream> OpenWriteTaskAsync(Uri address, string method);
        /// <summary>Uploads a data buffer to a resource identified by a URI.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <param name="data">The data buffer to send to the resource.</param>
        /// <returns>A <see cref="T:System.Byte"></see> array containing the body of the response from the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="data">data</paramref> is null.   -or-   An error occurred while sending the data.   -or-   There was no response from the server hosting the resource.</exception>
         byte[] UploadData(string address, byte[] data);
        /// <summary>Uploads a data buffer to the specified resource, using the specified method.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <param name="method">The HTTP method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The data buffer to send to the resource.</param>
        /// <returns>A <see cref="T:System.Byte"></see> array containing the body of the response from the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="data">data</paramref> is null.   -or-   An error occurred while uploading the data.   -or-   There was no response from the server hosting the resource.</exception>
         byte[] UploadData(string address, string method, byte[] data);
        /// <summary>Uploads a data buffer to a resource identified by a URI.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <param name="data">The data buffer to send to the resource.</param>
        /// <returns>A <see cref="T:System.Byte"></see> array containing the body of the response from the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="data">data</paramref> is null.   -or-   An error occurred while sending the data.   -or-   There was no response from the server hosting the resource.</exception>
         byte[] UploadData(Uri address, byte[] data);
        /// <summary>Uploads a data buffer to the specified resource, using the specified method.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <param name="method">The HTTP method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The data buffer to send to the resource.</param>
        /// <returns>A <see cref="T:System.Byte"></see> array containing the body of the response from the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="data">data</paramref> is null.   -or-   An error occurred while uploading the data.   -or-   There was no response from the server hosting the resource.</exception>
         byte[] UploadData(Uri address, string method, byte[] data);
        /// <summary>Uploads a data buffer to a resource identified by a URI, using the POST method. This method does not block the calling thread.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <param name="data">The data buffer to send to the resource.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while opening the stream.   -or-   There was no response from the server hosting the resource.</exception>
         void UploadDataAsync(Uri address, byte[] data);
        /// <summary>Uploads a data buffer to a resource identified by a URI, using the specified method. This method does not block the calling thread.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The data buffer to send to the resource.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while opening the stream.   -or-   There was no response from the server hosting the resource.</exception>
         void UploadDataAsync(Uri address, string method, byte[] data);
        /// <summary>Uploads a data buffer to a resource identified by a URI, using the specified method and identifying token.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The data buffer to send to the resource.</param>
        /// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while opening the stream.   -or-   There was no response from the server hosting the resource.</exception>
         void UploadDataAsync(Uri address, string method, byte[] data, object userToken);
        /// <summary>Uploads a data buffer that contains a <see cref="T:System.Byte"></see> array to the URI specified as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <param name="data">The data buffer to send to the resource.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.Byte"></see> array containing the body of the response received from the resource when the data buffer was uploaded.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while opening the stream.   -or-   There was no response from the server hosting the resource.</exception>
         Task<byte[]> UploadDataTaskAsync(string address, byte[] data);
        /// <summary>Uploads a data buffer that contains a <see cref="T:System.Byte"></see> array to the URI specified as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The data buffer to send to the resource.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.Byte"></see> array containing the body of the response received from the resource when the data buffer was uploaded.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while opening the stream.   -or-   There was no response from the server hosting the resource.</exception>
         Task<byte[]> UploadDataTaskAsync(string address, string method, byte[] data);
        /// <summary>Uploads a data buffer that contains a <see cref="T:System.Byte"></see> array to the URI specified as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <param name="data">The data buffer to send to the resource.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.Byte"></see> array containing the body of the response received from the resource when the data buffer was uploaded.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while opening the stream.   -or-   There was no response from the server hosting the resource.</exception>
         Task<byte[]> UploadDataTaskAsync(Uri address, byte[] data);
        /// <summary>Uploads a data buffer that contains a <see cref="T:System.Byte"></see> array to the URI specified as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the data.</param>
        /// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The data buffer to send to the resource.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.Byte"></see> array containing the body of the response received from the resource when the data buffer was uploaded.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while opening the stream.   -or-   There was no response from the server hosting the resource.</exception>
         Task<byte[]> UploadDataTaskAsync(Uri address, string method, byte[] data);
        /// <summary>Uploads the specified local file to a resource with the specified URI.</summary>
        /// <param name="address">The URI of the resource to receive the file. For example, ftp://localhost/samplefile.txt.</param>
        /// <param name="fileName">The file to send to the resource. For example, "samplefile.txt".</param>
        /// <returns>A <see cref="T:System.Byte"></see> array containing the body of the response from the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="fileName">fileName</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="fileName">fileName</paramref> is null, is <see cref="F:System.String.Empty"></see>, contains invalid characters, or does not exist.   -or-   An error occurred while uploading the file.   -or-   There was no response from the server hosting the resource.   -or-   The Content-type header begins with multipart.</exception>
         byte[] UploadFile(string address, string fileName);
        /// <summary>Uploads the specified local file to the specified resource, using the specified method.</summary>
        /// <param name="address">The URI of the resource to receive the file.</param>
        /// <param name="method">The method used to send the file to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="fileName">The file to send to the resource.</param>
        /// <returns>A <see cref="T:System.Byte"></see> array containing the body of the response from the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="fileName">fileName</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="fileName">fileName</paramref> is null, is <see cref="F:System.String.Empty"></see>, contains invalid characters, or does not exist.   -or-   An error occurred while uploading the file.   -or-   There was no response from the server hosting the resource.   -or-   The Content-type header begins with multipart.</exception>
         byte[] UploadFile(string address, string method, string fileName);
        /// <summary>Uploads the specified local file to a resource with the specified URI.</summary>
        /// <param name="address">The URI of the resource to receive the file. For example, ftp://localhost/samplefile.txt.</param>
        /// <param name="fileName">The file to send to the resource. For example, "samplefile.txt".</param>
        /// <returns>A <see cref="T:System.Byte"></see> array containing the body of the response from the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="fileName">fileName</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="fileName">fileName</paramref> is null, is <see cref="F:System.String.Empty"></see>, contains invalid characters, or does not exist.   -or-   An error occurred while uploading the file.   -or-   There was no response from the server hosting the resource.   -or-   The Content-type header begins with multipart.</exception>
         byte[] UploadFile(Uri address, string fileName);
        /// <summary>Uploads the specified local file to the specified resource, using the specified method.</summary>
        /// <param name="address">The URI of the resource to receive the file.</param>
        /// <param name="method">The method used to send the file to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="fileName">The file to send to the resource.</param>
        /// <returns>A <see cref="T:System.Byte"></see> array containing the body of the response from the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="fileName">fileName</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="fileName">fileName</paramref> is null, is <see cref="F:System.String.Empty"></see>, contains invalid characters, or does not exist.   -or-   An error occurred while uploading the file.   -or-   There was no response from the server hosting the resource.   -or-   The Content-type header begins with multipart.</exception>
         byte[] UploadFile(Uri address, string method, string fileName);
        /// <summary>Uploads the specified local file to the specified resource, using the POST method. This method does not block the calling thread.</summary>
        /// <param name="address">The URI of the resource to receive the file. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
        /// <param name="fileName">The file to send to the resource.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="fileName">fileName</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="fileName">fileName</paramref> is null, is <see cref="F:System.String.Empty"></see>, contains invalid character, or the specified path to the file does not exist.   -or-   An error occurred while opening the stream.   -or-   There was no response from the server hosting the resource.   -or-   The Content-type header begins with multipart.</exception>
         void UploadFileAsync(Uri address, string fileName);
        /// <summary>Uploads the specified local file to the specified resource, using the POST method. This method does not block the calling thread.</summary>
        /// <param name="address">The URI of the resource to receive the file. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
        /// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="fileName">The file to send to the resource.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="fileName">fileName</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="fileName">fileName</paramref> is null, is <see cref="F:System.String.Empty"></see>, contains invalid character, or the specified path to the file does not exist.   -or-   An error occurred while opening the stream.   -or-   There was no response from the server hosting the resource.   -or-   The Content-type header begins with multipart.</exception>
         void UploadFileAsync(Uri address, string method, string fileName);
        /// <summary>Uploads the specified local file to the specified resource, using the POST method. This method does not block the calling thread.</summary>
        /// <param name="address">The URI of the resource to receive the file. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
        /// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="fileName">The file to send to the resource.</param>
        /// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="fileName">fileName</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="fileName">fileName</paramref> is null, is <see cref="F:System.String.Empty"></see>, contains invalid character, or the specified path to the file does not exist.   -or-   An error occurred while opening the stream.   -or-   There was no response from the server hosting the resource.   -or-   The Content-type header begins with multipart.</exception>
         void UploadFileAsync(Uri address, string method, string fileName, object userToken);
        /// <summary>Uploads the specified local file to a resource as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the file. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
        /// <param name="fileName">The local file to send to the resource.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.Byte"></see> array containing the body of the response received from the resource when the file was uploaded.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="fileName">fileName</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="fileName">fileName</paramref> is null, is <see cref="F:System.String.Empty"></see>, contains invalid character, or the specified path to the file does not exist.   -or-   An error occurred while opening the stream.   -or-   There was no response from the server hosting the resource.   -or-   The Content-type header begins with multipart.</exception>
         Task<byte[]> UploadFileTaskAsync(string address, string fileName);
        /// <summary>Uploads the specified local file to a resource as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the file. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
        /// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="fileName">The local file to send to the resource.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.Byte"></see> array containing the body of the response received from the resource when the file was uploaded.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="fileName">fileName</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="fileName">fileName</paramref> is null, is <see cref="F:System.String.Empty"></see>, contains invalid character, or the specified path to the file does not exist.   -or-   An error occurred while opening the stream.   -or-   There was no response from the server hosting the resource.   -or-   The Content-type header begins with multipart.</exception>
         Task<byte[]> UploadFileTaskAsync(string address, string method, string fileName);
        /// <summary>Uploads the specified local file to a resource as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the file. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
        /// <param name="fileName">The local file to send to the resource.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.Byte"></see> array containing the body of the response received from the resource when the file was uploaded.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="fileName">fileName</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="fileName">fileName</paramref> is null, is <see cref="F:System.String.Empty"></see>, contains invalid character, or the specified path to the file does not exist.   -or-   An error occurred while opening the stream.   -or-   There was no response from the server hosting the resource.   -or-   The Content-type header begins with multipart.</exception>
         Task<byte[]> UploadFileTaskAsync(Uri address, string fileName);
        /// <summary>Uploads the specified local file to a resource as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the file. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
        /// <param name="method">The method used to send the data to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="fileName">The local file to send to the resource.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.Byte"></see> array containing the body of the response received from the resource when the file was uploaded.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="fileName">fileName</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="fileName">fileName</paramref> is null, is <see cref="F:System.String.Empty"></see>, contains invalid character, or the specified path to the file does not exist.   -or-   An error occurred while opening the stream.   -or-   There was no response from the server hosting the resource.   -or-   The Content-type header begins with multipart.</exception>
         Task<byte[]> UploadFileTaskAsync(Uri address, string method, string fileName);
        /// <summary>Uploads the specified string to the specified resource, using the POST method.</summary>
        /// <param name="address">The URI of the resource to receive the string. For Http resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
        /// <param name="data">The string to be uploaded.</param>
        /// <returns>A <see cref="T:System.String"></see> containing the response sent by the server.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   There was no response from the server hosting the resource.</exception>
         string UploadString(string address, string data);
        /// <summary>Uploads the specified string to the specified resource, using the specified method.</summary>
        /// <param name="address">The URI of the resource to receive the string. This URI must identify a resource that can accept a request sent with the method method.</param>
        /// <param name="method">The HTTP method used to send the string to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The string to be uploaded.</param>
        /// <returns>A <see cref="T:System.String"></see> containing the response sent by the server.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   There was no response from the server hosting the resource.   -or-  <paramref name="method">method</paramref> cannot be used to send content.</exception>
         string UploadString(string address, string method, string data);
        /// <summary>Uploads the specified string to the specified resource, using the POST method.</summary>
        /// <param name="address">The URI of the resource to receive the string. For Http resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
        /// <param name="data">The string to be uploaded.</param>
        /// <returns>A <see cref="T:System.String"></see> containing the response sent by the server.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   There was no response from the server hosting the resource.</exception>
         string UploadString(Uri address, string data);
        /// <summary>Uploads the specified string to the specified resource, using the specified method.</summary>
        /// <param name="address">The URI of the resource to receive the string. This URI must identify a resource that can accept a request sent with the method method.</param>
        /// <param name="method">The HTTP method used to send the string to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The string to be uploaded.</param>
        /// <returns>A <see cref="T:System.String"></see> containing the response sent by the server.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   There was no response from the server hosting the resource.   -or-  <paramref name="method">method</paramref> cannot be used to send content.</exception>
         string UploadString(Uri address, string method, string data);
        /// <summary>Uploads the specified string to the specified resource. This method does not block the calling thread.</summary>
        /// <param name="address">The URI of the resource to receive the string. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
        /// <param name="data">The string to be uploaded.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   There was no response from the server hosting the resource.</exception>
         void UploadStringAsync(Uri address, string data);
        /// <summary>Uploads the specified string to the specified resource. This method does not block the calling thread.</summary>
        /// <param name="address">The URI of the resource to receive the string. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
        /// <param name="method">The HTTP method used to send the file to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The string to be uploaded.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="method">method</paramref> cannot be used to send content.   -or-   There was no response from the server hosting the resource.</exception>
         void UploadStringAsync(Uri address, string method, string data);
        /// <summary>Uploads the specified string to the specified resource. This method does not block the calling thread.</summary>
        /// <param name="address">The URI of the resource to receive the string. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
        /// <param name="method">The HTTP method used to send the file to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The string to be uploaded.</param>
        /// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="method">method</paramref> cannot be used to send content.   -or-   There was no response from the server hosting the resource.</exception>
         void UploadStringAsync(Uri address, string method, string data, object userToken);
        /// <summary>Uploads the specified string to the specified resource as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the string. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
        /// <param name="data">The string to be uploaded.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.String"></see> containing the response sent by the server.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   There was no response from the server hosting the resource.</exception>
         Task<string> UploadStringTaskAsync(string address, string data);
        /// <summary>Uploads the specified string to the specified resource as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the string. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
        /// <param name="method">The HTTP method used to send the file to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The string to be uploaded.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.String"></see> containing the response sent by the server.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="method">method</paramref> cannot be used to send content.   -or-   There was no response from the server hosting the resource.</exception>
         Task<string> UploadStringTaskAsync(string address, string method, string data);
        /// <summary>Uploads the specified string to the specified resource as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the string. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
        /// <param name="data">The string to be uploaded.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.String"></see> containing the response sent by the server.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   There was no response from the server hosting the resource.</exception>
         Task<string> UploadStringTaskAsync(Uri address, string data);
        /// <summary>Uploads the specified string to the specified resource as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the string. For HTTP resources, this URI must identify a resource that can accept a request sent with the POST method, such as a script or ASP page.</param>
        /// <param name="method">The HTTP method used to send the file to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The string to be uploaded.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.String"></see> containing the response sent by the server.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="method">method</paramref> cannot be used to send content.   -or-   There was no response from the server hosting the resource.</exception>
         Task<string> UploadStringTaskAsync(Uri address, string method, string data);
        /// <summary>Uploads the specified name/value collection to the resource identified by the specified URI.</summary>
        /// <param name="address">The URI of the resource to receive the collection.</param>
        /// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection"></see> to send to the resource.</param>
        /// <returns>A <see cref="T:System.Byte"></see> array containing the body of the response from the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="data">data</paramref> is null.   -or-   There was no response from the server hosting the resource.   -or-   An error occurred while opening the stream.   -or-   The Content-type header is not null or "application/x-www-form-urlencoded".</exception>
         byte[] UploadValues(string address, NameValueCollection data);
        /// <summary>Uploads the specified name/value collection to the resource identified by the specified URI, using the specified method.</summary>
        /// <param name="address">The URI of the resource to receive the collection.</param>
        /// <param name="method">The HTTP method used to send the file to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection"></see> to send to the resource.</param>
        /// <returns>A <see cref="T:System.Byte"></see> array containing the body of the response from the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="data">data</paramref> is null.   -or-   An error occurred while opening the stream.   -or-   There was no response from the server hosting the resource.   -or-   The Content-type header value is not null and is not application/x-www-form-urlencoded.</exception>
         byte[] UploadValues(string address, string method, NameValueCollection data);
        /// <summary>Uploads the specified name/value collection to the resource identified by the specified URI.</summary>
        /// <param name="address">The URI of the resource to receive the collection.</param>
        /// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection"></see> to send to the resource.</param>
        /// <returns>A <see cref="T:System.Byte"></see> array containing the body of the response from the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="data">data</paramref> is null.   -or-   There was no response from the server hosting the resource.   -or-   An error occurred while opening the stream.   -or-   The Content-type header is not null or "application/x-www-form-urlencoded".</exception>
         byte[] UploadValues(Uri address, NameValueCollection data);
        /// <summary>Uploads the specified name/value collection to the resource identified by the specified URI, using the specified method.</summary>
        /// <param name="address">The URI of the resource to receive the collection.</param>
        /// <param name="method">The HTTP method used to send the file to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection"></see> to send to the resource.</param>
        /// <returns>A <see cref="T:System.Byte"></see> array containing the body of the response from the resource.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="data">data</paramref> is null.   -or-   An error occurred while opening the stream.   -or-   There was no response from the server hosting the resource.   -or-   The Content-type header value is not null and is not application/x-www-form-urlencoded.</exception>
         byte[] UploadValues(Uri address, string method, NameValueCollection data);
        /// <summary>Uploads the data in the specified name/value collection to the resource identified by the specified URI. This method does not block the calling thread.</summary>
        /// <param name="address">The URI of the resource to receive the collection. This URI must identify a resource that can accept a request sent with the default method.</param>
        /// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection"></see> to send to the resource.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   There was no response from the server hosting the resource.</exception>
         void UploadValuesAsync(Uri address, NameValueCollection data);
        /// <summary>Uploads the data in the specified name/value collection to the resource identified by the specified URI, using the specified method. This method does not block the calling thread.</summary>
        /// <param name="address">The URI of the resource to receive the collection. This URI must identify a resource that can accept a request sent with the method method.</param>
        /// <param name="method">The method used to send the string to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection"></see> to send to the resource.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   There was no response from the server hosting the resource.   -or-  <paramref name="method">method</paramref> cannot be used to send content.</exception>
         void UploadValuesAsync(Uri address, string method, NameValueCollection data);
        /// <summary>Uploads the data in the specified name/value collection to the resource identified by the specified URI, using the specified method. This method does not block the calling thread, and allows the caller to pass an object to the method that is invoked when the operation completes.</summary>
        /// <param name="address">The URI of the resource to receive the collection. This URI must identify a resource that can accept a request sent with the method method.</param>
        /// <param name="method">The HTTP method used to send the string to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection"></see> to send to the resource.</param>
        /// <param name="userToken">A user-defined object that is passed to the method invoked when the asynchronous operation completes.</param>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see> and <paramref name="address">address</paramref> is invalid.   -or-   There was no response from the server hosting the resource.   -or-  <paramref name="method">method</paramref> cannot be used to send content.</exception>
         void UploadValuesAsync(
          Uri address,
          string method,
          NameValueCollection data,
          object userToken);
        /// <summary>Uploads the specified name/value collection to the resource identified by the specified URI as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the collection.</param>
        /// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection"></see> to send to the resource.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.Byte"></see> array containing the response sent by the server.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-   There was no response from the server hosting the resource.   -or-   An error occurred while opening the stream.   -or-   The Content-type header is not null or "application/x-www-form-urlencoded".</exception>
         Task<byte[]> UploadValuesTaskAsync(string address, NameValueCollection data);
        /// <summary>Uploads the specified name/value collection to the resource identified by the specified URI as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the collection.</param>
        /// <param name="method">The HTTP method used to send the collection to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection"></see> to send to the resource.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.Byte"></see> array containing the response sent by the server.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="method">method</paramref> cannot be used to send content.   -or-   There was no response from the server hosting the resource.   -or-   An error occurred while opening the stream.   -or-   The Content-type header is not null or "application/x-www-form-urlencoded".</exception>
         Task<byte[]> UploadValuesTaskAsync(
          string address,
          string method,
          NameValueCollection data);
        /// <summary>Uploads the specified name/value collection to the resource identified by the specified URI as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the collection.</param>
        /// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection"></see> to send to the resource.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.Byte"></see> array containing the response sent by the server.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-   An error occurred while opening the stream.   -or-   There was no response from the server hosting the resource.   -or-   The Content-type header value is not null and is not application/x-www-form-urlencoded.</exception>
         Task<byte[]> UploadValuesTaskAsync(Uri address, NameValueCollection data);
        /// <summary>Uploads the specified name/value collection to the resource identified by the specified URI as an asynchronous operation using a task object.</summary>
        /// <param name="address">The URI of the resource to receive the collection.</param>
        /// <param name="method">The HTTP method used to send the collection to the resource. If null, the default is POST for http and STOR for ftp.</param>
        /// <param name="data">The <see cref="T:System.Collections.Specialized.NameValueCollection"></see> to send to the resource.</param>
        /// <returns>Returns <see cref="T:System.Threading.Tasks.Task`1"></see>.   The task object representing the asynchronous operation. The <see cref="P:System.Threading.Tasks.Task`1.Result"></see> property on the task object returns a <see cref="T:System.Byte"></see> array containing the response sent by the server.</returns>
        /// <exception cref="T:System.ArgumentNullException">The <paramref name="address">address</paramref> parameter is null.   -or-   The <paramref name="data">data</paramref> parameter is null.</exception>
        /// <exception cref="T:System.Net.WebException">The URI formed by combining <see cref="P:System.Net.WebClient.BaseAddress"></see>, and <paramref name="address">address</paramref> is invalid.   -or-  <paramref name="method">method</paramref> cannot be used to send content.   -or-   There was no response from the server hosting the resource.   -or-   An error occurred while opening the stream.   -or-   The Content-type header is not null or "application/x-www-form-urlencoded".</exception>
         Task<byte[]> UploadValuesTaskAsync(Uri address, string method, NameValueCollection data);
    }
}
