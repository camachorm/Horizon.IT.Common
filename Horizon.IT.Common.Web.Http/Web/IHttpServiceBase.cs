﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Horizon.IT.Common.Web.Http.Web
{
    public interface IHttpServiceBase
    {
        Task<JustHttpResponse<TOut>> GenericRequest<TOut>(ILogger logger,
            IEnumerable<KeyValuePair<string, string>> headers, Uri endpointUri, Func<TOut> executionFunc);

        JustHttpResponse<TOut> CarryOutRequest<TOut>(ILogger logger, IEnumerable<KeyValuePair<string, string>> headers,
            Uri endpointUri, Func<TOut> executionFunc);
    }
}