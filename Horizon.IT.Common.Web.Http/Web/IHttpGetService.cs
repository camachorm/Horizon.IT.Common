﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;
using Horizon.IT.Common.Abstractions.Interfaces;
using Horizon.IT.Common.Services;
using Microsoft.Extensions.Logging;

namespace Horizon.IT.Common.Web.Http.Web
{

    public interface IHttpGetService: 
        IService<JustHttpResponse<string>, Uri, IEnumerable<KeyValuePair<string, string>>>, // Simple Get, no parameters
        IService<JustHttpResponse<string>, Uri, IEnumerable<KeyValuePair<string, string>>, NameValueCollection>, // Simple Get with query string params
        IService<JustHttpResponse<string>, Uri, IEnumerable<KeyValuePair<string, string>>, IEnumerable<Tuple<string, string>>>, // Simple Get with query string params
        IService<JustHttpResponse<string>, Uri, IEnumerable<KeyValuePair<string, string>>, Dictionary<string, string>> // Simple Get with query string params
    {
        Task<JustHttpResponse<string>> GetStringAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers);
        Task<JustHttpResponse<string>> GetStringAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers, IEnumerable<Tuple<string, string>> queryParams);
        Task<JustHttpResponse<string>> GetStringAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers, Dictionary<string, string> queryParams);
        Task<JustHttpResponse<string>> GetStringAsync(ILogger logger, IIndexedConfiguration config, Uri endpointUrl, IEnumerable<KeyValuePair<string, string>> headers, NameValueCollection queryParams);
    }
}

