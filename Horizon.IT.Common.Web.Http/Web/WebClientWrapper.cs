﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Threading.Tasks;

namespace Horizon.IT.Common.Web.Http.Web
{
    [ExcludeFromCodeCoverage]
    public class WebClientWrapper : IWebClient
    {
        private readonly WebClient _client;
        
        public  WebClientWrapper(WebClient client)
        {
            _client = client;
        }


        public WebClientWrapper() : this(null)
        {
        }

        public virtual  string BaseAddress
        {
            get
            {
                if (_client == null) throw new NotImplementedException();
                return _client?.BaseAddress;
            }
            set
            {
                if (_client == null) throw new NotImplementedException();
                _client.BaseAddress = value;
            }
        }

        public virtual  RequestCachePolicy CachePolicy
        {
            get
            {
                if (_client == null) throw new NotImplementedException();
                return _client?.CachePolicy;
            }
            set
            {
                if (_client == null) throw new NotImplementedException();
                _client.CachePolicy = value;
            }
        }

        public virtual  ICredentials Credentials
        {
            get
            {
                if (_client == null) throw new NotImplementedException();
                return _client?.Credentials;
            }
            set
            {
                if (_client == null) throw new NotImplementedException();
                _client.Credentials = value;
            }
        }

        public virtual Encoding Encoding
        {
            get
            {
                if (_client == null) throw new NotImplementedException();
                return _client?.Encoding;
            }
            set
            {
                if (_client == null) throw new NotImplementedException();
                _client.Encoding = value;
            }
        }

        public virtual WebHeaderCollection Headers
        {
            get
            {
                if (_client == null) throw new NotImplementedException();
                return _client?.Headers ;
            }
            set
            {
                if (_client == null) throw new NotImplementedException();
                _client.Headers = value;
            }
        }

        public virtual bool IsBusy
        {
            get
            {
                if (_client == null) throw new NotImplementedException();
                return _client.IsBusy;
            }
        }

        public virtual IWebProxy Proxy
        {
            get
            {
                if (_client == null) throw new NotImplementedException();
                return _client?.Proxy;
            }
            set
            {
                if (_client == null) throw new NotImplementedException();
                _client.Proxy = value;
            }
        }

        public virtual NameValueCollection QueryString
        {
            get
            {
                if (_client == null) throw new NotImplementedException();
                return _client?.QueryString;
            }
            set
            {
                if (_client == null) throw new NotImplementedException();
                _client.QueryString = value;
            }
        }

        public virtual WebHeaderCollection ResponseHeaders
        {
            get
            {
                if (_client == null) throw new NotImplementedException();
                return _client?.ResponseHeaders;
            }
        }

        public virtual bool UseDefaultCredentials
        {
            get
            {
                if (_client == null) throw new NotImplementedException();
                return _client.UseDefaultCredentials;
            }
            set
            {
                if (_client == null) throw new NotImplementedException();
                _client.UseDefaultCredentials = value;
            }
        }
#pragma warning disable 67
        public virtual event DownloadDataCompletedEventHandler DownloadDataCompleted;
        public virtual event AsyncCompletedEventHandler DownloadFileCompleted;
        public virtual event DownloadProgressChangedEventHandler DownloadProgressChanged;
        public virtual event DownloadStringCompletedEventHandler DownloadStringCompleted;
        public virtual event OpenReadCompletedEventHandler OpenReadCompleted;
        public virtual event OpenWriteCompletedEventHandler OpenWriteCompleted;
        public virtual event UploadDataCompletedEventHandler UploadDataCompleted;
        public virtual event UploadFileCompletedEventHandler UploadFileCompleted;
        public virtual event UploadProgressChangedEventHandler UploadProgressChanged;
        public virtual event UploadStringCompletedEventHandler UploadStringCompleted;
        public virtual event UploadValuesCompletedEventHandler UploadValuesCompleted;
#pragma warning restore 67
        public virtual void CancelAsync()
        {
            if (_client == null) throw new NotImplementedException();
            _client.CancelAsync();
        }

        public virtual byte[] DownloadData(string address)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.DownloadData(address);
        }

        public virtual byte[] DownloadData(Uri address)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.DownloadData(address);
        }

        public virtual void DownloadDataAsync(Uri address)
        {
            if (_client == null) throw new NotImplementedException();
            _client.DownloadDataAsync(address);
        }

        public virtual void DownloadDataAsync(Uri address, object userToken)
        {
            if (_client == null) throw new NotImplementedException();
            _client.DownloadDataAsync(address, userToken);
        }

        public virtual async Task<byte[]> DownloadDataTaskAsync(string address)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.DownloadDataTaskAsync(address);
        }

        public virtual async Task<byte[]> DownloadDataTaskAsync(Uri address)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.DownloadDataTaskAsync(address);
        }

        public virtual void DownloadFile(string address, string fileName)
        {
            if (_client == null) throw new NotImplementedException();
            _client.DownloadFile(address, fileName);
        }

        public virtual void DownloadFile(Uri address, string fileName)
        {
            if (_client == null) throw new NotImplementedException();
            _client.DownloadFile(address, fileName);
        }

        public virtual void DownloadFileAsync(Uri address, string fileName)
        {
            if (_client == null) throw new NotImplementedException();
            _client.DownloadFileAsync(address, fileName);
        }

        public virtual void DownloadFileAsync(Uri address, string fileName, object userToken)
        {
            if (_client == null) throw new NotImplementedException();
            _client.DownloadFileAsync(address, fileName);
        }

        public virtual async Task DownloadFileTaskAsync(string address, string fileName)
        {
            if (_client == null) throw new NotImplementedException();
            await _client.DownloadFileTaskAsync(address, fileName);
        }

        public virtual async Task DownloadFileTaskAsync(Uri address, string fileName)
        {
            if (_client == null) throw new NotImplementedException();
            await _client.DownloadFileTaskAsync(address, fileName);
        }

        public virtual string DownloadString(string address)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.DownloadString(address);
        }

        public virtual string DownloadString(Uri address)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.DownloadString(address);
        }

        public virtual void DownloadStringAsync(Uri address)
        {
            if (_client == null) throw new NotImplementedException();
            _client.DownloadString(address);
        }

        public virtual void DownloadStringAsync(Uri address, object userToken)
        {
            if (_client == null) throw new NotImplementedException();
            _client.DownloadStringAsync(address, userToken);
        }

        public virtual async Task<string> DownloadStringTaskAsync(string address)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.DownloadStringTaskAsync(address);
        }

        public virtual async Task<string> DownloadStringTaskAsync(Uri address)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.DownloadStringTaskAsync(address);
        }

        public virtual Stream OpenRead(string address)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.OpenRead(address);
        }

        public virtual Stream OpenRead(Uri address)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.OpenRead(address);
        }

        public virtual void OpenReadAsync(Uri address)
        {
            if (_client == null) throw new NotImplementedException();
            _client.OpenReadAsync(address);
        }

        public virtual void OpenReadAsync(Uri address, object userToken)
        {
            if (_client == null) throw new NotImplementedException();
            _client.OpenReadAsync(address, userToken);
        }

        public virtual async Task<Stream> OpenReadTaskAsync(string address)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.OpenReadTaskAsync(address);
        }

        public virtual async Task<Stream> OpenReadTaskAsync(Uri address)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.OpenReadTaskAsync(address);
        }

        public virtual Stream OpenWrite(string address)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.OpenWrite(address);
        }

        public virtual Stream OpenWrite(string address, string method)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.OpenWrite(address, method);
        }

        public virtual Stream OpenWrite(Uri address)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.OpenWrite(address);
        }

        public virtual Stream OpenWrite(Uri address, string method)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.OpenWrite(address, method);
        }

        public virtual void OpenWriteAsync(Uri address)
        {
            if (_client == null) throw new NotImplementedException();
            _client.OpenWriteAsync(address);
        }

        public virtual void OpenWriteAsync(Uri address, string method)
        {
            if (_client == null) throw new NotImplementedException();
            _client.OpenWriteAsync(address, method);
        }

        public virtual void OpenWriteAsync(Uri address, string method, object userToken)
        {
            if (_client == null) throw new NotImplementedException();
            _client.OpenWriteAsync(address, method, userToken);
        }

        public virtual async Task<Stream> OpenWriteTaskAsync(string address)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.OpenWriteTaskAsync(address);
        }

        public virtual async Task<Stream> OpenWriteTaskAsync(string address, string method)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.OpenWriteTaskAsync(address, method);
        }

        public virtual async Task<Stream> OpenWriteTaskAsync(Uri address)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.OpenWriteTaskAsync(address);
        }

        public virtual async Task<Stream> OpenWriteTaskAsync(Uri address, string method)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.OpenWriteTaskAsync(address, method);
        }

        public virtual byte[] UploadData(string address, byte[] data)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.UploadData(address, data);
        }

        public virtual byte[] UploadData(string address, string method, byte[] data)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.UploadData(address, method, data);
        }

        public virtual byte[] UploadData(Uri address, byte[] data)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.UploadData(address, data);
        }

        public virtual byte[] UploadData(Uri address, string method, byte[] data)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.UploadData(address, method, data);
        }

        public virtual void UploadDataAsync(Uri address, byte[] data)
        {
            if (_client == null) throw new NotImplementedException();
            _client.UploadDataAsync(address, data);
        }

        public virtual void UploadDataAsync(Uri address, string method, byte[] data)
        {
            if (_client == null) throw new NotImplementedException();
            _client.UploadDataAsync(address, method, data);
        }

        public virtual void UploadDataAsync(Uri address, string method, byte[] data, object userToken)
        {
            if (_client == null) throw new NotImplementedException();
            _client.UploadDataAsync(address, method, data, userToken);
        }

        public virtual async Task<byte[]> UploadDataTaskAsync(string address, byte[] data)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.UploadDataTaskAsync(address, data);
        }

        public virtual async Task<byte[]> UploadDataTaskAsync(string address, string method, byte[] data)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.UploadDataTaskAsync(address, method, data);
        }

        public virtual async Task<byte[]> UploadDataTaskAsync(Uri address, byte[] data)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.UploadDataTaskAsync(address, data);
        }

        public virtual async Task<byte[]> UploadDataTaskAsync(Uri address, string method, byte[] data)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.UploadDataTaskAsync(address, method, data);
        }

        public virtual byte[] UploadFile(string address, string fileName)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.UploadFile(address, fileName);
        }

        public virtual byte[] UploadFile(string address, string method, string fileName)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.UploadFile(address, method, fileName);
        }

        public virtual byte[] UploadFile(Uri address, string fileName)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.UploadFile(address, fileName);
        }

        public virtual byte[] UploadFile(Uri address, string method, string fileName)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.UploadFile(address, method, fileName);
        }

        public virtual void UploadFileAsync(Uri address, string fileName)
        {
            if (_client == null) throw new NotImplementedException();
            _client.UploadFileAsync(address, fileName);
        }

        public virtual void UploadFileAsync(Uri address, string method, string fileName)
        {
            if (_client == null) throw new NotImplementedException();
            _client.UploadFileAsync(address, method, fileName);
        }

        public virtual void UploadFileAsync(Uri address, string method, string fileName, object userToken)
        {
            if (_client == null) throw new NotImplementedException();
            _client.UploadFileAsync(address, method, fileName, userToken);
        }

        public virtual async Task<byte[]> UploadFileTaskAsync(string address, string fileName)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.UploadFileTaskAsync(address, fileName);
        }

        public virtual async Task<byte[]> UploadFileTaskAsync(string address, string method, string fileName)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.UploadFileTaskAsync(address, method, fileName);
        }

        public virtual async Task<byte[]> UploadFileTaskAsync(Uri address, string fileName)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.UploadFileTaskAsync(address, fileName);
        }

        public virtual async Task<byte[]> UploadFileTaskAsync(Uri address, string method, string fileName)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.UploadFileTaskAsync(address, method, fileName);
        }

        public virtual string UploadString(string address, string data)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.UploadString(address, data);
        }

        public virtual string UploadString(string address, string method, string data)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.UploadString(address, method, data);
        }

        public virtual string UploadString(Uri address, string data)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.UploadString(address, data);
        }

        public virtual string UploadString(Uri address, string method, string data)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.UploadString(address, method, data);
        }

        public virtual void UploadStringAsync(Uri address, string data)
        {
            if (_client == null) throw new NotImplementedException();
            _client.UploadStringAsync(address, data);
        }

        public virtual void UploadStringAsync(Uri address, string method, string data)
        {
            if (_client == null) throw new NotImplementedException();
            _client.UploadStringAsync(address, method, data);
        }

        public virtual void UploadStringAsync(Uri address, string method, string data, object userToken)
        {
            if (_client == null) throw new NotImplementedException();
            _client.UploadStringAsync(address, method, data, userToken);
        }

        public virtual async Task<string> UploadStringTaskAsync(string address, string data)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.UploadStringTaskAsync(address,  data);
        }

        public virtual async Task<string> UploadStringTaskAsync(string address, string method, string data)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.UploadStringTaskAsync(address, method, data);
        }

        public virtual async Task<string> UploadStringTaskAsync(Uri address, string data)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.UploadStringTaskAsync(address,  data);
        }

        public virtual async Task<string> UploadStringTaskAsync(Uri address, string method, string data)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.UploadStringTaskAsync(address, method, data);
        }

        public virtual byte[] UploadValues(string address, NameValueCollection data)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.UploadValues(address,  data);
        }

        public virtual byte[] UploadValues(string address, string method, NameValueCollection data)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.UploadValues(address, method,  data);
        }

        public virtual byte[] UploadValues(Uri address, NameValueCollection data)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.UploadValues(address,  data);
        }

        public virtual byte[] UploadValues(Uri address, string method, NameValueCollection data)
        {
            if (_client == null) throw new NotImplementedException();
            return _client.UploadValues(address, method,  data);
        }

        public virtual void UploadValuesAsync(Uri address, NameValueCollection data)
        {
            if (_client == null) throw new NotImplementedException();
            _client.UploadValuesAsync(address, data);
        }

        public virtual void UploadValuesAsync(Uri address, string method, NameValueCollection data)
        {
            if (_client == null) throw new NotImplementedException();
            _client.UploadValuesAsync(address, method, data);
        }

        public virtual void UploadValuesAsync(Uri address, string method, NameValueCollection data, object userToken)
        {
            if (_client == null) throw new NotImplementedException();
            _client.UploadValuesAsync(address, method, data, userToken);
        }

        public virtual async Task<byte[]> UploadValuesTaskAsync(string address, NameValueCollection data)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.UploadValuesTaskAsync(address,  data);
        }

        public virtual async Task<byte[]> UploadValuesTaskAsync(string address, string method, NameValueCollection data)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.UploadValuesTaskAsync(address, method,  data);
        }

        public virtual async Task<byte[]> UploadValuesTaskAsync(Uri address, NameValueCollection data)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.UploadValuesTaskAsync(address,  data);
        }

        public virtual async Task<byte[]> UploadValuesTaskAsync(Uri address, string method, NameValueCollection data)
        {
            if (_client == null) throw new NotImplementedException();
            return await _client.UploadValuesTaskAsync(address, method,  data);
        }

        public static implicit operator WebClientWrapper(WebClient client)
        {
            return new WebClientWrapper(client);
        }
    }
}