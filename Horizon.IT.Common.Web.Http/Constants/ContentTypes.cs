﻿using System.Collections.Generic;

namespace Horizon.IT.Common.Web.Http.Constants
{
    public static class ContentTypes
    {
        public const string AcceptHeader = "Accept";
        public const string ContentTypeHeader = "Content-Type";
        public const string ApplicationXml = "Application/Xml";
        public const string ApplicationJson = "Application/Json";

        public static KeyValuePair<string, string> ContentTypeApplicationJsonKeyValuePair = new KeyValuePair<string, string>(ContentTypeHeader, ApplicationJson);
        public static KeyValuePair<string, string> ContentTypeApplicationXmlKeyValuePair = new KeyValuePair<string, string>(ContentTypeHeader, ApplicationXml);
        public static KeyValuePair<string, string> AcceptApplicationJsonKeyValuePair = new KeyValuePair<string, string>(AcceptHeader, ApplicationJson);
    }
}