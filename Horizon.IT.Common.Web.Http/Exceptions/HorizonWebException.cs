﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Runtime.Serialization;

namespace Horizon.IT.Common.Exceptions
{
    [ExcludeFromCodeCoverage]
    public class HorizonWebException : Exception
    {
        public HorizonWebException()
        {
        }

        public HorizonWebException(string message) : base(message)
        {
        }

        public HorizonWebException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected HorizonWebException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public WebHeaderCollection ResponseHeaders { get; set; }
    }
}
