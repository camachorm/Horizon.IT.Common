﻿using Microsoft.Extensions.Logging;
using Xunit.Abstractions;

namespace Horizon.IT.Common.xUnit.Logging.xUnit
{
    // ReSharper disable UnusedMember.Global
    // ReSharper disable InconsistentNaming
    public class xUnitLoggerProvider : ILoggerProvider
    // ReSharper restore InconsistentNaming
    // ReSharper restore UnusedMember.Global
    {
        private readonly ITestOutputHelper _testOutput;

        public xUnitLoggerProvider(ITestOutputHelper testOutput)
        {
            _testOutput = testOutput;
        }

        public void Dispose()
        {
        }

        public ILogger CreateLogger(string categoryName)
        {
            return new xUnitLogger(categoryName, _testOutput);
        }
    }
}
