﻿using System;
using System.Diagnostics.CodeAnalysis;
using Horizon.IT.Common.Mocks;
using Microsoft.Extensions.Logging;
using Xunit.Abstractions;

// ReSharper disable LocalizableElement

namespace Horizon.IT.Common.xUnit.Logging.xUnit
{
    // ReSharper disable InconsistentNaming
    // ReSharper disable UnusedMember.Global
    [ExcludeFromCodeCoverage]
    public class xUnitLogger : ILogger
    // ReSharper restore UnusedMember.Global
    // ReSharper restore InconsistentNaming
    {
        public string CategoryName { get; }
        private readonly ITestOutputHelper _testOutput;

        public xUnitLogger(string categoryName, ITestOutputHelper testOutput)
        {
            CategoryName = categoryName;
            _testOutput = testOutput;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            // This is required for the VS Output window
            _testOutput.WriteLine(formatter.Invoke(state, exception));
            // This is required for the build pipeline logging
            Console.WriteLine($"xUnit.Console: {formatter.Invoke(state,exception)}");
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return new DisposableScope();
        }
    }
}
