﻿using Microsoft.Extensions.Logging;
using Xunit.Abstractions;

namespace Horizon.IT.Common.xUnit.Logging.xUnit
{
    // ReSharper disable InconsistentNaming
    // ReSharper disable UnusedMember.Global
    public static class xUnitLoggerExtensions
    // ReSharper restore UnusedMember.Global
    // ReSharper restore InconsistentNaming
    {
        public static ILoggerFactory AddXUnitLogger(this ILoggerFactory factory, ITestOutputHelper testOutput)
        {
            factory.AddProvider(new xUnitLoggerProvider(testOutput));
            return factory;
        }
    }
}
