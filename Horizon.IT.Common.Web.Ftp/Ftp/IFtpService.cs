﻿using System;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;
using FluentFTP;
using Horizon.IT.Common.Abstractions.Interfaces;
using Horizon.IT.Common.Services;
using Microsoft.Extensions.Logging;

// ReSharper disable UnusedMember.Global

namespace Horizon.IT.Common.Web.Ftp.Ftp
{
    public interface IFtpService : 
        IService<bool, string, string, byte[], FtpExists, bool, IProgress<FtpProgress>, CancellationToken>,
        IService<string, string, CancellationToken>,
        // NOTE: We exclude CancellationToken from the signature to avoid ambiguity with IService<string, string, CancellationToken>
        IService<byte[], string>,
        IService<bool, string, OperationTargetType, CancellationToken>
    {
        void Connect(ILogger logger, IIndexedConfiguration config, string host, string user, string password,
            int port = 21, SslProtocols protocol = SslProtocols.Tls12,
            FtpEncryptionMode encryptionMode = FtpEncryptionMode.Explicit,
            FtpDataConnectionType dataConnectionType = FtpDataConnectionType.PASV,
            bool dataConnectionEncryption = true);

        Task ConnectAsync(ILogger logger, IIndexedConfiguration config, string host, string user, string password,
            int port = 21, SslProtocols protocol = SslProtocols.Tls12,
            FtpEncryptionMode encryptionMode = FtpEncryptionMode.Explicit,
            FtpDataConnectionType dataConnectionType = FtpDataConnectionType.PASV,
            bool dataConnectionEncryption = true);

        bool WriteFile(ILogger logger, IIndexedConfiguration config, string folderPath, string fileName,
            byte[] fileContent, FtpExists exists = FtpExists.Overwrite, bool createRemoteDir = false,
            IProgress<FtpProgress> progress = null, CancellationToken token = default(CancellationToken));

        Task<bool> WriteFileAsync(ILogger logger, IIndexedConfiguration config, string folderPath, string fileName,
            byte[] fileContent, FtpExists exists = FtpExists.Overwrite, bool createRemoteDir = false,
            IProgress<FtpProgress> progress = null, CancellationToken token = default(CancellationToken));

        string ReadFileAsString(ILogger logger, IIndexedConfiguration config, string fileFullPath,
            CancellationToken token);

        Task<string> ReadFileAsStringAsync(ILogger logger, IIndexedConfiguration config, string fileFullPath,
            CancellationToken token);

        byte[] ReadFileAsBinary(ILogger logger, IIndexedConfiguration config, string fileFullPath,
            CancellationToken token);

        Task<byte[]> ReadFileAsBinaryAsync(ILogger logger, IIndexedConfiguration config, string fileFullPath,
            CancellationToken token);

        bool Exists(ILogger logger, IIndexedConfiguration config, string fileFullPath, OperationTargetType operationTargetType);

        Task<bool> ExistsAsync(ILogger logger, IIndexedConfiguration config, string fileFullPath, OperationTargetType operationTargetType, CancellationToken token);
        
        bool Delete(ILogger logger, IIndexedConfiguration config, string fileFullPath, OperationTargetType operationTargetType);

        Task<bool> DeleteAsync(ILogger logger, IIndexedConfiguration config, string fileFullPath, OperationTargetType operationTargetType, CancellationToken token);

        IEnumerable<FtpListItem> ListFolder(ILogger logger, IIndexedConfiguration config, string fullPath, FtpListOption option = FtpListOption.AllFiles);

        Task<IEnumerable<FtpListItem>> ListFolderAsync(ILogger logger, IIndexedConfiguration config, string fullPath, CancellationToken token, FtpListOption option = FtpListOption.AllFiles);

    }

    public enum OperationTargetType
    {
        File,
        Directory
    }
}
