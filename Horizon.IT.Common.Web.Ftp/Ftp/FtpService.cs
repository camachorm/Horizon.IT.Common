﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Net;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;
using FluentFTP;
using Horizon.IT.Common.Abstractions.Interfaces;
using Horizon.IT.Common.Extensions;
using Horizon.IT.Common.Services;
using Microsoft.Extensions.Logging;

namespace Horizon.IT.Common.Web.Ftp.Ftp
{
    // TODO: We exclude this from code coverage as it is currently reporting coverage wrongly on most of the covered methods until we can properly investigate and identify causes for that
    [ExcludeFromCodeCoverage]
    public class FtpService : IFtpService
    {
        private readonly IFtpClient _client;

        public FtpService(IFtpClient client)
        {
            _client = client;
        }

        #region Connection Methods

        public void Connect(ILogger logger, IIndexedConfiguration config, string host, string user, string password,
            int port = 21, SslProtocols protocol = SslProtocols.Tls12,
            FtpEncryptionMode encryptionMode = FtpEncryptionMode.Explicit,
            FtpDataConnectionType dataConnectionType = FtpDataConnectionType.PASV, bool dataConnectionEncryption = true)
        {
            var t = ConnectAsync(logger, config, host, user, password, port, protocol, encryptionMode,
                dataConnectionType, dataConnectionEncryption);
            t.Wait();
        }

        public async Task ConnectAsync(ILogger logger, IIndexedConfiguration config, string host, string user,
            string password, int port = 21, SslProtocols protocol = SslProtocols.Tls12,
            FtpEncryptionMode encryptionMode = FtpEncryptionMode.Explicit,
            FtpDataConnectionType dataConnectionType = FtpDataConnectionType.PASV, bool dataConnectionEncryption = true)
        {
            _client.Host = host;
            _client.Port = port;
            _client.Credentials = new NetworkCredential(user, password);
            _client.SslProtocols = protocol;
            _client.EncryptionMode = encryptionMode;
            _client.DataConnectionType = dataConnectionType;
            _client.DataConnectionEncryption = dataConnectionEncryption;
            logger.LogTrace(
                $"Connecting to Host: {host}, Port: {port}, User: {user}, Password: {password.ToPrintableSecret(5, 1)}, SslProtocol: {protocol}, EncryptionMode: {_client.EncryptionMode}");
            await _client.ConnectAsync();
            logger.LogTrace(
                $"Connected to Host: {host}, Port: {port}, User: {user}, Password: {password.ToPrintableSecret(5, 1)}, SslProtocol: {protocol}, EncryptionMode: {_client.EncryptionMode}");

            
        }

        #endregion

        #region Write File
        [ExcludeFromCodeCoverage]
        public bool WriteFile(ILogger logger, IIndexedConfiguration config, string folderPath, string fileName,
           byte[] fileContent, FtpExists exists = FtpExists.Overwrite, bool createRemoteDir = false, IProgress<FtpProgress> progress = null,
           CancellationToken token = default(CancellationToken))
        {
            var t = WriteFileAsync(logger, config, folderPath, fileName, fileContent, exists, createRemoteDir, progress,
                token);
            t.Wait(token);
            return t.Result;
        }

        [ExcludeFromCodeCoverage]
        public async Task<bool> WriteFileAsync(ILogger logger, IIndexedConfiguration config, string folderPath, string fileName,
            byte[] fileContent, FtpExists exists = FtpExists.Overwrite, bool createRemoteDir = false, IProgress<FtpProgress> progress = null,
            CancellationToken token = default(CancellationToken))
        {
            logger.LogTrace($"Writing to Folder: {folderPath}, File: {fileName}, Content Size: {fileContent.Length}");
            logger.LogTrace($"\tWriting Behaviour Settings FtpExists: {exists}, createRemoteDir: {createRemoteDir}, IProgress<FtpProgress>: {progress}");
            return await _client.UploadAsync(new MemoryStream(fileContent), Path.Combine(folderPath, fileName), exists,
                createRemoteDir, progress, token);
        }

        #endregion

        #region Read File
        [ExcludeFromCodeCoverage]
        public string ReadFileAsString(ILogger logger, IIndexedConfiguration config, string fileFullPath,
           CancellationToken token)
        {
            var t = ReadFileAsStringAsync(logger, config, fileFullPath, token);
            t.Wait(token);
            return t.Result;
        }

        [ExcludeFromCodeCoverage]
        public async Task<string> ReadFileAsStringAsync(ILogger logger, IIndexedConfiguration config, string fileFullPath,
            CancellationToken token)
        {
            logger.LogTrace($"Reading File As String: '{fileFullPath}'");
            using (var stream = await _client.OpenReadAsync(fileFullPath, token))
            {
                using (var sr = new StreamReader(stream))
                {
                    return await sr.ReadToEndAsync();
                }
            }
        }
        
        [ExcludeFromCodeCoverage]
        public byte[] ReadFileAsBinary(ILogger logger, IIndexedConfiguration config, string fileFullPath,
            CancellationToken token)
        {
            var t = ReadFileAsBinaryAsync(logger, config, fileFullPath, token);
            t.Wait(token);
            return t.Result;
        }

        [ExcludeFromCodeCoverage]
        public async Task<byte[]> ReadFileAsBinaryAsync(ILogger logger, IIndexedConfiguration config, string fileFullPath,
            CancellationToken token)
        {
            logger.LogTrace($"Reading File As Binary: '{fileFullPath}'");
            using (var stream = await _client.OpenReadAsync(fileFullPath, token))
            {
                using (var ms = new MemoryStream())
                {
                    await stream.CopyToAsync(ms);
                    return ms.ToArray();
                }
            }
        }
        #endregion

        #region Check File

        [ExcludeFromCodeCoverage]
        public bool Exists(ILogger logger, IIndexedConfiguration config, string path, OperationTargetType operationTargetType)
        {
            var t = ExistsAsync(logger, config, path, operationTargetType, default);
            t.Wait();
            return t.Result;
        }

        [ExcludeFromCodeCoverage]
        public async Task<bool> ExistsAsync(ILogger logger, IIndexedConfiguration config, string path, OperationTargetType operationTargetType, CancellationToken token)
        {
            logger.LogTrace($"Checking if {operationTargetType}, Exists as {path}");
            return operationTargetType == OperationTargetType.Directory
                ? await _client.DirectoryExistsAsync(path, token)
                : await _client.FileExistsAsync(path, token);
        }

        #endregion

        #region Delete File

        [ExcludeFromCodeCoverage]
        public bool Delete(ILogger logger, IIndexedConfiguration config, string path, OperationTargetType operationTargetType)
        {
            var t = DeleteAsync(logger, config, path, operationTargetType, default);
            t.Wait();
            return t.Result;
        }

        [ExcludeFromCodeCoverage]
        public async Task<bool> DeleteAsync(ILogger logger, IIndexedConfiguration config, string fileFullPath, OperationTargetType operationTargetType, CancellationToken cancellationToken)
        {
            logger.LogTrace($"Deleting {operationTargetType}, Exists as {fileFullPath}");
            if (OperationTargetType.Directory == operationTargetType)
                await _client.DeleteDirectoryAsync(fileFullPath, cancellationToken);
            else
                await _client.DeleteFileAsync(fileFullPath, cancellationToken);
            return true;
        }

        public IEnumerable<FtpListItem> ListFolder(ILogger logger, IIndexedConfiguration config, string fullPath, FtpListOption option = FtpListOption.AllFiles)
        {
            var t = ListFolderAsync(logger, config, fullPath, default, option);
            t.Wait();
            return t.Result;
        }

        public async Task<IEnumerable<FtpListItem>> ListFolderAsync(ILogger logger, IIndexedConfiguration config, string fullPath, CancellationToken token, FtpListOption option = FtpListOption.AllFiles)
        {
            return await _client.GetListingAsync(fullPath, option, token);
        }

        #endregion

        #region IService Implementations

        [ExcludeFromCodeCoverage]
        async Task<bool> IService<bool, string, string, byte[], FtpExists, bool, IProgress<FtpProgress>, CancellationToken>.ExecuteAsync(
            ILogger logger,
            IIndexedConfiguration config,
            string folderPath,
            string fileName,
            byte[] fileContent,
            FtpExists exists,
            bool createRemoteDir,
            IProgress<FtpProgress> progress,
            CancellationToken token)
        {
            return await WriteFileAsync(logger, config, folderPath, fileName, fileContent, exists, createRemoteDir, progress,
                token);
        }

        [ExcludeFromCodeCoverage]
        async Task<string> IService<string, string, CancellationToken>.ExecuteAsync(ILogger logger, IIndexedConfiguration config, string fileFullPath, CancellationToken token)
        {
            return await ReadFileAsStringAsync(logger, config, fileFullPath, token);
        }

        // NOTE: We exclude CancellationToken from the signature to avoid ambiguity with IService<string, string, CancellationToken>
        [ExcludeFromCodeCoverage]
        async Task<byte[]> IService<byte[], string>.ExecuteAsync(ILogger logger, IIndexedConfiguration config, string fileFullPath)
        {
            return await ReadFileAsBinaryAsync(logger, config, fileFullPath, default(CancellationToken));
        }

        [ExcludeFromCodeCoverage]
        public async Task<bool> ExecuteAsync(ILogger logger, IIndexedConfiguration config, string path, OperationTargetType operationTargetType, CancellationToken token)
        {
            return await ExistsAsync(logger, config, path, operationTargetType, token);
        }

        #endregion
    }
}
