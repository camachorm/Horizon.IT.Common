﻿using System;
using System.Collections.Generic;
using System.Threading;
using Horizon.IT.Common.Abstractions.Interfaces;
using Horizon.IT.Common.Business.Models;
using Microsoft.Extensions.Logging;

namespace Horizon.IT.Common.Services.Calendar
{
    public interface ICalendarService : 
        IService<int, int, int, CancellationToken> ,
        IService<DateTime, DateTime, CancellationToken, bool> , 
        IService<bool, DateTime>, 
        IService<List<BankHoliday>>
    {
        int GetFirstWorkingDayOfMonth(ILogger logger, IIndexedConfiguration config, int month, int year, CancellationToken token);

        DateTime GetPreviousWorkingDay(ILogger logger, IIndexedConfiguration config, DateTime fromDate, CancellationToken token, bool includeCurrentDay = false);

        List<BankHoliday> GetPublicHolidays(ILogger logger, IIndexedConfiguration config, CancellationToken token);

        bool IsPublicHoliday(ILogger logger, IIndexedConfiguration config, DateTime date, CancellationToken token);

        DateTime GetIncomeIssueDate(ILogger logger, IIndexedConfiguration config, int month, int year, CancellationToken token);
    }
}
