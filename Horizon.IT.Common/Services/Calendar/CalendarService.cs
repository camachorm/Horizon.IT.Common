﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Horizon.IT.Common.Abstractions.Interfaces;
using Horizon.IT.Common.Business.Dates;
using Horizon.IT.Common.Business.Models;
using Horizon.IT.Common.Extensions;
using Microsoft.Extensions.Logging;

// ReSharper disable UnusedMember.Global

namespace Horizon.IT.Common.Services.Calendar
{
    public class CalendarService : ICalendarService
    {
        private readonly IBankHolidays _bankHolidays;


        public CalendarService(ILogger logger = null):this(new BankHolidays(logger ?? new LoggerFactory().CreateLogger("CalendarServiceDebugLogger")))
        {
        }

        public CalendarService(IBankHolidays bankHolidays)
        {
            _bankHolidays = bankHolidays;
        }

        public int GetFirstWorkingDayOfMonth(ILogger logger, IIndexedConfiguration config, int month, int year, CancellationToken token)
        {
            var t = ExecuteAsync(logger, config, month, year, token);
            t.Wait(token);
            return t.Result;
        }

        public DateTime GetPreviousWorkingDay(ILogger logger, IIndexedConfiguration config, DateTime fromDate, CancellationToken token, bool includeCurrentDay = false)
        {
            var t = ExecuteAsync(logger, config, fromDate, token, includeCurrentDay);
            t.Wait(token);
            return t.Result;
        }

        public List<BankHoliday> GetPublicHolidays(ILogger logger, IIndexedConfiguration config, CancellationToken token)
        {
            var t = ExecuteAsync(logger, config);
            t.Wait(token);
            return t.Result;
        }

        public bool IsPublicHoliday(ILogger logger, IIndexedConfiguration config, DateTime date, CancellationToken token)
        {
            var t = ExecuteAsync(logger, config, date);
            t.Wait(token);
            return t.Result;
        }

        public DateTime GetIncomeIssueDate(ILogger logger, IIndexedConfiguration config, int month, int year, CancellationToken token)
        {
            var t = ExecuteAsync(logger, config, month, year);
            t.Wait(token);
            return t.Result;
        }

        public Task<DateTime> ExecuteAsync(ILogger logger, IIndexedConfiguration config, int month, int year)
        {
            logger.LogTrace("Getting Income Issue Date");
            var incomeAvailableDate = new DateTime(year, month, 1).AddMonths(1);
            DateTime incomeIssueDate;
            var counter = 1;
            while (_bankHolidays.IsBankHoliday(incomeAvailableDate) || incomeAvailableDate.IsWeekend())
            {
                incomeAvailableDate = incomeAvailableDate.AddDays(-1);
            }
            incomeIssueDate = incomeAvailableDate;
            while (counter <= 5)
            {
                incomeIssueDate = incomeIssueDate.AddDays(-1);

                if (!(_bankHolidays.IsBankHoliday(incomeIssueDate) || incomeIssueDate.IsWeekend()))
                {
                    counter += 1;
                }
            }

            return Task.FromResult(incomeIssueDate);
        }

        public Task<int> ExecuteAsync(ILogger logger, IIndexedConfiguration config, int month, int year, CancellationToken input3)
        {
            logger.LogTrace("Getting First Working Day Of Month");
            var calcDate = new DateTime(year, month, 1);

            while (_bankHolidays.IsBankHoliday(calcDate) || calcDate.IsWeekend())
            {
                calcDate = calcDate.AddDays(1);
            }

            return Task.FromResult(calcDate.Day);
        }

        public Task<DateTime> ExecuteAsync(ILogger logger, IIndexedConfiguration config, DateTime fromDate, CancellationToken input3, bool includeCurrentDay = false)
        {
            logger.LogTrace("Getting Previous Working Day");

            if(!includeCurrentDay)
                fromDate = fromDate.AddDays(-1);

            while (_bankHolidays.IsBankHoliday(fromDate) || fromDate.IsWeekend())
            {
                fromDate = fromDate.AddDays(-1);
            }

            return Task.FromResult(fromDate);
        }

        public Task<bool> ExecuteAsync(ILogger logger, IIndexedConfiguration config, DateTime date)
        {
            logger.LogTrace("Validate Public Holiday");
            return Task.FromResult(_bankHolidays.IsBankHoliday(date));
        }

        public Task<List<BankHoliday>> ExecuteAsync(ILogger logger, IIndexedConfiguration config)
        {
            logger.LogTrace("Get Public Holidays");
            return Task.FromResult(_bankHolidays.GetBankHolidays());
        }
    }
}
