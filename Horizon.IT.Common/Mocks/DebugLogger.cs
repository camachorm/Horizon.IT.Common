﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Logging;

namespace Horizon.IT.Common.Mocks
{
    /// <summary>
    /// This class writes to Console, DebugLogger and Trace
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class DebugLogger : ILogger
    {
        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception,
            Func<TState, Exception, string> formatter)
        {
            Console.WriteLine(
                $"Level: {logLevel}, eventId: {eventId}, state: {state}, exception: {exception?.Message}, formatter: {formatter.Invoke(state, exception)}");
            System.Diagnostics.Debug.WriteLine($"Level: {logLevel}, eventId: {eventId}, state: {state}, exception: {exception?.Message}, formatter: {formatter.Invoke(state, exception)}");
            Trace.TraceInformation($"Level: {logLevel}, eventId: {eventId}, state: {state}, exception: {exception?.Message}, formatter: {formatter.Invoke(state, exception)}");
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return new DisposableScope();
        }
    }
}