﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Horizon.IT.Common.Abstractions.Configuration;

// ReSharper disable UnusedMember.Global

namespace Horizon.IT.Common.Mocks
{
    [ExcludeFromCodeCoverage]
    public class MockConfiguration : IndexedConfigurationBase
    {
        private readonly Dictionary<string, string> _valuesDictionary = new Dictionary<string, string>();

        public override string this[string key]
        {
            get => _valuesDictionary[key];
            set
            {
                if (!_valuesDictionary.ContainsKey(key))
                    _valuesDictionary.Add(key, value);
                else
                    _valuesDictionary[key] = value;
            }
        }

        public override IEnumerable<string> Keys => _valuesDictionary.Keys;
    }
}