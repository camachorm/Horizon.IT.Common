﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Horizon.IT.Common.Mocks
{
    [ExcludeFromCodeCoverage]
    public class DisposableScope : IDisposable
    {
        public void Dispose()
        {
        }
    }
}