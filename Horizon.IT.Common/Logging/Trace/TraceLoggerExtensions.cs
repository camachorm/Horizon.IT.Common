﻿using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Logging;

// ReSharper disable UnusedMember.Global

namespace Horizon.IT.Common.Logging.Trace
{
    [ExcludeFromCodeCoverage]
    public static class TraceLoggerExtensions
    {

        public static ILoggerFactory AddTraceLogger(this ILoggerFactory factory, ITraceLoggerConfiguration configuration)
        {
            factory.AddProvider(new TraceLoggerProvider(configuration));
            
            return factory;
        }

        public static ILoggerFactory AddTraceLogger(this ILoggerFactory factory, 
            Action<ITraceLoggerConfiguration> configure)
        {
            var configuration = new TraceLoggerConfiguration();
            
            configure(configuration);

            return factory.AddTraceLogger(configuration);
        }

        public static ILoggerFactory AddTraceLogger(this ILoggerFactory factory)
        {
            return factory.AddTraceLogger(new TraceLoggerConfiguration());
        }
    }
}
