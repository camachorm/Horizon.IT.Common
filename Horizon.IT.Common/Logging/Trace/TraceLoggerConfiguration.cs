﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Logging;

namespace Horizon.IT.Common.Logging.Trace
{
    [ExcludeFromCodeCoverage]
    public class TraceLoggerConfiguration : ITraceLoggerConfiguration
    {
        public LogLevel LogLevel { get; set; } = LogLevel.Warning;
    }
}