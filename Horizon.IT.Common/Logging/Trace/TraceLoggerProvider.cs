﻿using System.Collections.Concurrent;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Logging;

namespace Horizon.IT.Common.Logging.Trace
{
    [ExcludeFromCodeCoverage]
    public class TraceLoggerProvider : ILoggerProvider
    {
        private readonly ITraceLoggerConfiguration _configuration;
        private readonly ConcurrentDictionary<string, TraceLogger> _loggers = new ConcurrentDictionary<string, TraceLogger>();

        public TraceLoggerProvider(ITraceLoggerConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Dispose()
        {
            _loggers.Clear();
        }

        public ILogger CreateLogger(string categoryName)
        {
            lock (_loggers)
            {
                return _loggers.GetOrAdd(categoryName, name => new TraceLogger(name, _configuration));
            }
        }
    }
}
