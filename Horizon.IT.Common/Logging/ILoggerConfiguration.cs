﻿using Microsoft.Extensions.Logging;

namespace Horizon.IT.Common.Logging
{
    public interface ILoggerConfiguration
    {
        LogLevel LogLevel { get; set; }
    }
}