﻿using System;
using System.Linq;
using System.Reflection;

namespace Horizon.IT.Common.Extensions
{
    public static class AssemblyExtensions
    {
        public static string ReadEmbeddedResource(this Assembly source, string resourceName)
        {
            var sourceStream = source.GetManifestResourceStream(resourceName);
            
            if (sourceStream == null) throw new ArgumentException(GenerateInvalidResourceExceptionMessage(source,resourceName), resourceName);

            return sourceStream.ReadToEnd();
        }

        private static string GenerateInvalidResourceExceptionMessage(this Assembly source, string resourceName)
        {
            var message =
                $@"Provided resource name:{System.Environment.NewLine}";
            message += $"\t({resourceName}) is not found in assembly:{System.Environment.NewLine}";
            message += $"\t\t{source.FullName}{System.Environment.NewLine}";
            message += $"Valid options are: {System.Environment.NewLine}";

            return source.GetManifestResourceNames().Aggregate(message, (current, manifestResourceName) => current + $"\t{manifestResourceName}{System.Environment.NewLine}");
        }
    }
}
