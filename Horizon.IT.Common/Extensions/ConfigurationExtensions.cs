﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Horizon.IT.Common.Abstractions.Configuration;
using Horizon.IT.Common.Abstractions.Interfaces;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;

namespace Horizon.IT.Common.Extensions
{
    [ExcludeFromCodeCoverage]
    public static class ConfigurationExtensions
    {
        /// <summary>
        /// Helper method that recursively loads a <see cref="IConfiguration"/> data into the current <see cref="IIndexedConfiguration"/> singleton by registering
        /// a new Provider of type <see cref="NameValueCollectionIndexedConfiguration"/> with all the entries contained there in
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public static IIndexedConfiguration AddMicrosoftIConfiguration(this IIndexedConfiguration configuration, IConfiguration source, Func<string, string, string> getIdentifierFunc = null)
        {
            if (source == null) return configuration;

            var allConfig = source.AllConfig();

            if (getIdentifierFunc == null) getIdentifierFunc = GetIdentifier;

            configuration.RootConfiguration.RegisterProvider(
                new NameValueCollectionIndexedConfiguration(allConfig.ToDictionary(tuple => $"{getIdentifierFunc(tuple.Item1, tuple.Item3)}", tuple => tuple.Item2).ToNameValueCollection()));

            return configuration;
        }

        /// <summary>
        /// Reads a configuration entry with key <see cref="configName"/> from <see cref="source"/>  and censors its content, displaying a maximum of <see cref="maxChars"/>
        /// chars at the start and end of the text.
        /// </summary>
        /// <param name="source">The configuration instance to use</param>
        /// <param name="configName">The key identifier of the desired configuration entry</param>
        /// <param name="maxChars">The maximum number of characters to display at either end of the string</param>
        /// <param name="filters">The list of filters to apply. If none is provided (e.g. filters = null), then default filter is applied (e.g. filters = new[] {"KeyVaultIndexedConfiguration"})</param>
        /// <returns>The censored value of the provided <see cref="configName"/></returns>
        public static string ReadConfigAndCensorSecrets(this IIndexedConfiguration source, string configName,
            int maxChars = 4, IEnumerable<string> filters = null)
        {
            if (filters == null) filters = new[] { "KeyVaultIndexedConfiguration" };

            var result = source[configName.FilterOutIIndexedConfigurationProvider()];

            return filters.Any(filter => configName.StartsWith(filter) || configName.EndsWith(filter)) ? result.ToPrintableSecret(30, maxChars) : result;
        }

        private static string GetIdentifier(string key, string path)
        {
            return path == key ? key : $"{path}-{key}";
        }

        public static IList<Tuple<string, string, string>> AllConfig(this IConfiguration configuration)
        {
            if (configuration == null) return new List<Tuple<string, string, string>>();

            var result = new List<Tuple<string, string, string>>();

            foreach (var section in configuration.GetChildren())
            {
                result.AddRange(section.AllConfig());
            }

            return result;
        }

        public static IList<Tuple<string, string, string>> AllConfig(this IConfigurationSection section)
        {
            if (section == null) return new List<Tuple<string, string, string>>();

            var result = new List<Tuple<string, string, string>> { new Tuple<string, string, string>(section.Key, section.Value, section.Path) };

            var children = section.GetChildren();

            foreach (var child in children)
            {
                result.AddRange(child.AllConfig());
            }

            return result;
        }

        /// <summary>
        /// Reads all the currently loaded configuration entries from <see cref="source"/> and censors its content based on the provided <see cref="filters"/>, displaying a maximum of <see cref="maxChars"/> 
        /// chars on the censured entries at the start and end of the text.
        /// </summary>
        /// <param name="source">The configuration instance to use</param>
        /// <param name="maxChars">The maximum number of characters to display at either end of the censored string</param>
        /// <param name="filters">The list of filters to apply. If none is provided (e.g. filters = null), then default filter is applied (e.g. filters = new[] {"KeyVaultIndexedConfiguration"})</param>
        /// <returns>A new JObject with multiple Properties with values being censored/uncensored depending on the provided <see cref="filters"/></returns>
        public static JObject ToJObject(this IIndexedConfiguration source, int maxChars = 4, IEnumerable<string> filters = null)
        {
            var configuration = new JObject();
            var keys = source.Keys;
            foreach (var key in keys)
            {
                // ReSharper disable PossibleMultipleEnumeration
                configuration.Add(key, source.ReadConfigAndCensorSecrets(key, maxChars, filters));
                // ReSharper restore PossibleMultipleEnumeration
            }

            return configuration;
        }
    }
}
