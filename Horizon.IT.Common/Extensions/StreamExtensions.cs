﻿using System.IO;

namespace Horizon.IT.Common.Extensions
{
    public static class StreamExtensions
    {
        public static string ReadToEnd(this Stream source)
        {
            return new StreamReader(source).ReadToEnd();
        }
    }
}
