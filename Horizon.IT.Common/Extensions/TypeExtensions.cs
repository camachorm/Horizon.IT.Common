﻿using System;

namespace Horizon.IT.Common.Extensions
{
    public static class TypeExtensions
    {
        public static string ReadEmbeddedResource(this Type source, string resourceName)
        {
            return source.Assembly.ReadEmbeddedResource(resourceName);
        }
    }
}
