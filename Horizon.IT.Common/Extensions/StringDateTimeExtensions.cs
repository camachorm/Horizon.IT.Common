﻿using System.Globalization;

namespace Horizon.IT.Common.Extensions
{
    public static class StringDateTimeExtensions
    {
        public static bool IsValidDateTime(this string source, string cultureInfoSettingsKey = "DateTimeCultureInfo",
            DateTimeStyles style = DateTimeStyles.None)
        {
            return source.ToDateTime(cultureInfoSettingsKey, style) != null;
        }
    }
}
