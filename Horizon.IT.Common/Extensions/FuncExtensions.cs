﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace Horizon.IT.Common.Extensions
{
    public static class FuncExtensions
    {
        public static async Task WrapTestOperation(this Func<Task> actionToInvoke, string keyToLog,
            Dictionary<string, JToken> log, ILogger logger)
        {
            try
            {
                await actionToInvoke.Invoke();
                log.Add(keyToLog, "Success");
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Unable to perform operation against {keyToLog}: {e.FullStackTrace()}");
                log.Add(keyToLog, e.ToErrorJToken());
            }
        }

        public static async Task<T> WrapTestOperation<T>(this Func<Task<T>> actionToInvoke, string keyToLog,
            Dictionary<string, JToken> log, ILogger logger)
        {
            T r = default;
            try
            {
                r = await actionToInvoke.Invoke();
                log.Add(keyToLog, new JObject().AddProperties(
                    new KeyValuePair<string, JToken>("Status", "Success"),
                    new KeyValuePair<string, JToken>("Result", r.ToSafeJToken(logger))));
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Unable to perform operation against {keyToLog}: {e.FullStackTrace()}");
                log.Add(keyToLog, e.ToErrorJToken());
            }

            return r;
        }

        public static T WrapTestOperation<T>(this Func<T> actionToInvoke, string keyToLog,
            Dictionary<string, JToken> log, ILogger logger)
        {
            T r = default;
            try
            {
                r = actionToInvoke.Invoke();
                log.Add(keyToLog, new JObject().AddProperties(
                    new KeyValuePair<string, JToken>("Status", "Success"),
                    new KeyValuePair<string, JToken>("Result", r.ToSafeJToken(logger))));
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Unable to perform operation against {keyToLog}: {e.FullStackTrace()}");
                log.Add(keyToLog, e.ToErrorJToken());
            }

            return r;
        }
    }
}
