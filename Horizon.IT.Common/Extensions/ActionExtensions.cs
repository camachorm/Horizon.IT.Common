﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace Horizon.IT.Common.Extensions
{
    public static class ActionExtensions
    {
        public static void WrapTestOperation(this Action actionToInvoke, string keyToLog,
            Dictionary<string, JToken> log, ILogger logger)
        {
            try
            {
                actionToInvoke.Invoke();
                log.Add(keyToLog, "Success");
            }
            catch (Exception e)
            {
                logger.LogError(e, $"Unable to perform operation against {keyToLog}: {e.FullStackTrace()}");
                log.Add(keyToLog, e.ToErrorJToken());
            }
        }
    }
}
