﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

// ReSharper disable UnusedMember.Global

namespace Horizon.IT.Common.Extensions
{
    public static class NewtonsoftExtensions
    {
        /// <summary>
        /// Converts a <see cref="JObject"/> to a <see cref="JToken"/>
        /// </summary>
        /// <param name="source">The <see cref="JObject"/> to convert</param>
        /// <returns>The converted <see cref="JToken"/></returns>
        public static JObject ToJObject(this JToken source)
        {
            return source as JObject;
        }

        /// <summary>
        /// Performs a <see cref="JArray"/>.Parse(str) call to convert a json string to an <see cref="JArray"/> instance
        /// </summary>
        /// <param name="collection">The <see cref="IEnumerable{T}"/> to parse</param>
        /// <returns>The parsed <see cref="JArray"/></returns>
        [ExcludeFromCodeCoverage]
        public static JArray ToJArray(this IEnumerable collection)
        {
            var result = new JArray();
            foreach (var item in collection)
            {
                result.Add(item.ToJToken());
            }
            return result;
        }

        /// <summary>
        /// Converts a <see cref="JObject"/> to a <see cref="T"/>
        /// </summary>
        /// <param name="source">The <see cref="T"/> to convert</param>
        /// <returns>The converted <see cref="JObject"/></returns>
        public static JObject ToJObject<T>(this T source)
        {
            return JObject.FromObject(source);
        }

        /// <summary>
        /// Converts an <see cref="IEnumerable{T}"/> to a <see cref="JToken"/> by calling <seealso cref="ToJArray"/>
        /// </summary>
        /// <typeparam name="T">The Type of <see cref="T"/></typeparam>
        /// <param name="source">The <see cref="IEnumerable{T}"/></param>
        /// <returns>a <see cref="JToken"/></returns>
        [ExcludeFromCodeCoverage]
        public static JToken ToJToken<T>(this IEnumerable<T> source)
        {
            return source.ToJArray();
        }

        /// <summary>
        /// Converts an <see cref="T"/> safely to a <see cref="JToken"/> by calling <seealso cref="ToJObject"/> within a try/catch block
        /// </summary>
        /// <typeparam name="T">The Type of <see cref="T"/></typeparam>
        /// <param name="source">The instance of <see cref="T"/></param>
        /// <param name="logger">The logger to be used</param>
        /// <returns>a <see cref="JToken"/></returns>
        [ExcludeFromCodeCoverage]
        public static JToken ToSafeJToken<T>(this T source, ILogger logger)
        {
            try
            {
                if (source is JToken token) return token;
                return source.ToJToken();
            }
            catch (Exception e)
            {
                logger.LogTrace($"Unable to jsonify the object {source}, will return null instead, this was due to: {e.FullStackTrace()}");
                return new JValue((object)null);
            }
        }

        /// <summary>
        /// Converts an <see cref="T"/> to a <see cref="JToken"/> by calling <seealso cref="ToJObject"/>
        /// </summary>
        /// <typeparam name="T">The Type of <see cref="T"/></typeparam>
        /// <param name="source">The instance of <see cref="T"/></param>
        /// <returns>a <see cref="JToken"/></returns>
        [ExcludeFromCodeCoverage]
        public static JToken ToJToken<T>(this T source)
        {
            // ReSharper disable once ConvertIfStatementToSwitchStatement
            if (source == null) return new JValue((object)null);
            if (source is IEnumerable enumerable)
            {
                return enumerable.ToJArray();
            }
            if (source.GetType().IsPrimitive)
            {
                return new JValue(source);
            }
            return source.ToJObject();
        }

        /// <summary>
        /// Converts a json <see cref="String"/> to a <see cref="T"/>
        /// </summary>
        /// <typeparam name="T">The object to deserialize json to</typeparam>
        /// <param name="jsonSource">The json string to deserialize</param>
        /// <returns>The deserialized object</returns>
        public static T ReadJsonStringToObject<T>(this string jsonSource)
        {
            return JsonConvert.DeserializeObject<T>(jsonSource);
        }

        /// <summary>
        /// Converts a <see cref="JObject"/> to a <see cref="String"/>
        /// </summary>
        /// <param name="source">The source <see cref="JObject"/></param>
        /// <param name="formatting">The <see cref="Formatting"/> to apply (defaults to Formatting.Indented)</param>
        /// <returns>The Json formatted <see cref="string"/></returns>
        public static string ToJson(this JToken source, Formatting formatting = Formatting.Indented)
        {
            return source.ToString(formatting);
        }

        /// <summary>
        /// Converts a <see cref="T"/> to a <see cref="String"/>
        /// </summary>
        /// <param name="source">The source <see cref="T"/></param>
        /// <param name="formatting">The <see cref="Formatting"/> to apply (defaults to Formatting.Indented)</param>
        /// <returns>The Json formatted <see cref="string"/></returns>
        public static string ToJson<T>(this T source, Formatting formatting = Formatting.Indented)
        {
            return source.ToJToken().ToJson(formatting);
        }

        /// <summary>
        /// Converts the value of a <see cref="JToken"/> to a nullable boolean by chaining a call to <see cref="SafeToString"/> and <see cref="StringExtensions.ToNullableBoolean(string)"/>
        /// </summary>
        /// <param name="source">The <see cref="JToken"/> to convert</param>
        /// <returns>The converted <see cref="Nullable"/> boolean</returns>
        public static bool? ToBoolean(this JToken source)
        {
            return source.SafeToString().ToNullableBoolean();
        }

        /// <summary>
        /// Converts the value of a <see cref="JToken"/> to a nullable datetime by chaining a call to <see cref="SafeToString"/> and <see cref="StringExtensions.ToDateTime(string, string, System.Globalization.DateTimeStyles)"/>
        /// </summary>
        /// <param name="source">The <see cref="JToken"/> to convert</param>
        /// <returns>The converted <see cref="Nullable"/> DateTime</returns>
        public static DateTime? ToDateTime(this JToken source)
        {
            return source.SafeToString().ToDateTime();
        }

        /// <summary>
        /// Converts the value of a <see cref="JToken"/> to a string, or null if token is null or token type is null
        /// </summary>
        /// <param name="source">The <see cref="JToken"/> to convert</param>
        /// <returns>The string or null</returns>
        public static string SafeToString(this JToken source)
        {
            return (source == null || source.Type == JTokenType.Null) ? null : source.ToString();
        }

        /// <summary>
        /// Safely reads the direct child token with the specified name from <see cref="JToken"/>
        /// </summary>
        /// <param name="source">The <see cref="JToken"/> to read from</param>
        /// <param name="tokenName">The name of the child token to read</param>
        /// <returns>The <see cref="JToken"/> if found, or null otherwise</returns>
        public static JToken ChildToken(this JToken source, string tokenName)
        {
            return source?[tokenName];
        }

        /// <summary>
        /// Tries to read the name of the <see cref="JToken"/> if possible, returns null otherwise
        /// </summary>
        /// <param name="source">The <see cref="JToken"/> to read from</param>
        /// <returns>The <see cref="JObject"/> Name property value if the JToken is of <see cref="JObject"/> type, null otherwise</returns>
        public static string ReadName(this JToken source)
        {
            switch (source.Type)
            {
                case JTokenType.Property:
                    return ((JProperty)source).Name;
                case JTokenType.Object:
                    var properties = source.ToJObject()?.Properties()?.ToList() ?? new List<JProperty>();
                    return properties.Count() == 1 ? properties.SingleOrDefault()?.Name : null;
                case JTokenType.Constructor:
                case JTokenType.Comment:
                case JTokenType.Integer:
                case JTokenType.Float:
                case JTokenType.String:
                case JTokenType.Boolean:
                case JTokenType.Null:
                case JTokenType.Undefined:
                case JTokenType.Date:
                case JTokenType.Raw:
                case JTokenType.Bytes:
                case JTokenType.Guid:
                case JTokenType.Uri:
                case JTokenType.TimeSpan:
                case JTokenType.Array:
                case JTokenType.None:
                    return null;
                default:
                    return null;
            }
        }

        /// <summary>
        /// Hierarchically navigate through the children until a matching token is found, returning it, or null if not found/
        /// </summary>
        /// <param name="source">The source <see cref="JToken"/> where to search for the child</param>
        /// <param name="tokenName">The name of the token to search for</param>
        /// <returns>The first child token with the requested name, or null if none found</returns>
        public static JToken FindToken(this JToken source, string tokenName)
        {
            // ReSharper disable ConstantConditionalAccessQualifier
            if ((!(source is JProperty) && source[tokenName] != null) || !(source?.Children().Any()).GetValueOrDefault()) return source?[tokenName];
            // ReSharper restore ConstantConditionalAccessQualifier

            foreach (var child in source.Children())
            {
                var result = child.FindToken(tokenName);
                if (result != null)
                    return result;
            }

            return null;
        }

        /// <summary>
        /// Dynamically add new properties to a <see cref="JObject"/>
        /// </summary>
        /// <param name="source">The <see cref="JObject"/> to add properties to</param>
        /// <param name="properties">The list of properties to be added in a <see cref="KeyValuePair{TKey,TValue}"/> format</param>
        /// <returns>The <see cref="source"/> <see cref="JObject"/> for fluent syntax.</returns>
        public static JObject AddProperties(this JObject source, params KeyValuePair<string, JToken>[] properties)
        {
            foreach (var property in properties)
            {
                source.Add(property.Key, property.Value);
            }
            return source;
        }

        /// <summary>
        /// Hierarchically navigate through the children returning all matching tokens
        /// </summary>
        /// <param name="source">The source <see cref="JToken"/> where to search for the child</param>
        /// <param name="tokenName">The name of the token to search for</param>
        /// <returns>All the child tokens with the requested name</returns>
        public static IEnumerable<JToken> FindTokens(this JToken source, string tokenName)
        {
            var result = new List<JToken>();

            if (source.ReadName() == tokenName && source.Path.EndsWith($".{tokenName}"))
            {
                Trace.TraceInformation($"Adding Node with Path: {source.Path}");
                result.Add(source);
            }

            foreach (var child in source?.Children() ?? new JEnumerable<JToken>())
            {
                result.AddRange(child.FindTokens(tokenName));
            }

            return result;
        }
    }
}
