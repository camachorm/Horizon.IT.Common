﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Runtime.Versioning;

// ReSharper disable UnusedMember.Global

namespace Horizon.IT.Common.Environment
{
    /// <summary>
    ///     This class provides static standardized access to information about the version of .Net being used at runtime
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class RuntimeInformation
    {
        private const string VersionPrefix = "Version=v";

        private static readonly string FrameworkName =
            Assembly.GetEntryAssembly()?.GetCustomAttribute<TargetFrameworkAttribute>().FrameworkName;

        private static readonly int VersionPrefixLength = VersionPrefix.Length;

        public static string Version =
            FrameworkName.Substring(FrameworkName.IndexOf(VersionPrefix, StringComparison.InvariantCulture) +
                                    VersionPrefixLength);

        public static bool IsDotNetFramework => FrameworkName.StartsWith(".NETFramework");
        public static bool IsDotNetCore => FrameworkName.StartsWith(".NETCoreApp");
        public static bool IsDotNetStandard => FrameworkName.StartsWith(".NETFramework");
    }
}