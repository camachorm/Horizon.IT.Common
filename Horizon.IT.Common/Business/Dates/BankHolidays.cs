﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using Horizon.IT.Common.Business.Models;
using Horizon.IT.Common.Extensions;
using Microsoft.Extensions.Logging;

namespace Horizon.IT.Common.Business.Dates
{
    public class BankHolidays : IBankHolidays
    {
        private const string EmbeddedResourceName = "Horizon.IT.Common.Resources.BankHolidays.csv";
        private readonly ILogger _logger;
        private List<BankHoliday> _holidays;

        [ExcludeFromCodeCoverage]
        public BankHolidays(ILogger logger) : this(
            GetBankHolidays(new List<BankHoliday>(),
                typeof(BankHolidays).ReadEmbeddedResource(EmbeddedResourceName), logger),
            logger)
        {
        }

        public BankHolidays(string holidaysCsv, ILogger logger) : this(GetBankHolidays(new List<BankHoliday>(), holidaysCsv, logger), logger)
        {
        }

        public BankHolidays(List<BankHoliday> holidays, ILogger logger)
        {
            _holidays = holidays;
            _logger = logger;
        }

        public List<BankHoliday> GetBankHolidays()
        {
            var fileContent = GetType().ReadEmbeddedResource(EmbeddedResourceName);
            _logger.LogTrace($"Loading resource File Holidays: {fileContent.Length}");
            return GetBankHolidays(fileContent);
        }

        public List<BankHoliday> GetBankHolidays(string holidaysCsv)
        {
            _logger.LogTrace($"Loading string Holidays: {holidaysCsv.Length}");
            return GetBankHolidays(_holidays, holidaysCsv, _logger);
        }

        private static List<BankHoliday> GetBankHolidays(List<BankHoliday> holidays, string holidaysCsv,
            ILogger logger)
        {
            logger.LogTrace($"Loaded Holidays: {(holidays?.Count).GetValueOrDefault(0)}");
            if (holidays?.Count > 0)
                return holidays;
            
            logger.LogTrace($"Holidays.Count == 0, Loading fresh from Csv with: {holidaysCsv.Length}bytes");

            var data = new List<BankHoliday>();

            using (var sr = new StringReader(holidaysCsv))
            {
                // Skip Headers Row
                sr.ReadLine();

                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    string[] rows = line.Split(',');

                    DateTime.TryParseExact(
                        rows.FirstOrDefault(x => x.Contains("/")),
                        "dd/MM/yyyy",
                        CultureInfo.InvariantCulture,
                        DateTimeStyles.None,
                        out var tempDate);

                    data.Add(new BankHoliday
                    {
                        Description = rows.FirstOrDefault(x => !x.Contains("/")),
                        Date = tempDate
                    });
                }
            }
            
            logger.LogTrace($"Loaded a total of: {data.Count} public holidays from csv {holidaysCsv.Length}");

            return data;
        }

        public bool IsBankHoliday(DateTime date)
        {
            if (_holidays == null)
                _holidays = GetBankHolidays();

            return (_holidays.Any(x => x.Date.Date == date.Date));
        }

        public void AddBankHoliday(DateTime date, string description)
        {
            _holidays.Add(new BankHoliday
            {
                Date = date,
                Description = description
            });
        }
    }
}
