﻿using System;
using System.Collections.Generic;
using Horizon.IT.Common.Business.Models;

namespace Horizon.IT.Common.Business.Dates
{
    public interface IBankHolidays
    {
        List<BankHoliday> GetBankHolidays();
        List<BankHoliday> GetBankHolidays(string holidaysCsv);
        bool IsBankHoliday(DateTime date);
        void AddBankHoliday(DateTime date, string description);
    }
}