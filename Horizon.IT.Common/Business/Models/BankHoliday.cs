﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Horizon.IT.Common.Business.Models
{
    [ExcludeFromCodeCoverage]
    public class BankHoliday
    {
        public string Description { get; set; }
        public DateTime Date { get; set; }
    }
}
