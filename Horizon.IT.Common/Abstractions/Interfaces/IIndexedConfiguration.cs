﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Horizon.IT.Common.Abstractions.Configuration;

// ReSharper disable UnusedMember.Global

namespace Horizon.IT.Common.Abstractions.Interfaces
{
    public interface IIndexedConfiguration
    {
        string this[string key] { get; set; }
        string ProviderId { get; }
        IEnumerable<string> List { get; }
        IEnumerable<string> Keys { get; }
        IEnumerable<string> ProviderList { get; }
        MultipleConfiguration RootConfiguration { get; }
        string ReadKey(string key);
        Task<string> ReadKeyAsync(string key);
    }
}