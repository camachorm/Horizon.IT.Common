﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Horizon.IT.Common.Abstractions.Interfaces;
using Microsoft.Extensions.Logging;

// ReSharper disable UnusedMember.Global
namespace Horizon.IT.Common.Abstractions.Configuration
{
    public class MultipleConfiguration : IndexedConfigurationBase
    {
        private static readonly ReaderWriterLock Lock = new ReaderWriterLock();

        public MultipleConfiguration()
        {

        }

        public MultipleConfiguration(params IIndexedConfiguration[] configurationProviders)
        {
            lock (Lock)
            {
                Logger.LogInformation($"Initializing new MultipleConfiguration with a total of {configurationProviders.Length} Providers.");
                ConfigurationProviders.AddRange(configurationProviders);
            }
        }

        public List<IIndexedConfiguration> ConfigurationProviders { get; } = new List<IIndexedConfiguration>();

        public override string this[string key]
        {
            get
            {
                lock (Lock)
                {
                    return (from provider in ConfigurationProviders where provider.Keys.Contains(key) select provider[key])
                        .FirstOrDefault(v => !string.IsNullOrEmpty(v));
                }
            }
            set
            {
                lock (Lock)
                {
                    foreach (var provider in ConfigurationProviders.Where(cp => cp != this && cp != RootConfiguration))
                        provider[key] = value;
                }
            }
        }

        public override IEnumerable<string> Keys
        {
            get
            {
                lock (Lock)
                {
                    return ConfigurationProviders.SelectMany(p => p.Keys.Select(k => $"{p.GetType().Name}:: {k}"));
                }
            }
        }

        public override IEnumerable<string> ProviderList
        {
            get
            {
                lock (Lock)
                {
                    return ConfigurationProviders.Select(cp => cp.GetType().Name);
                }
            }
        }

        public override IEnumerable<string> List
        {
            get
            {
                lock (Lock)
                {
                    return ConfigurationProviders.SelectMany(cp => cp.Keys.Select(k => $"{k}/{cp[k]}"));
                }
            }
        }

        public MultipleConfiguration RegisterProvider(IIndexedConfiguration newProvider)
        {
            lock (Lock)
            {
                if (ConfigurationProviders.Contains(newProvider) || RootConfiguration == newProvider) return this;

                Logger.LogInformation($"Adding new Provider: {newProvider.ProviderId}");
                
                ConfigurationProviders.Add(newProvider);
                
                return this;
            }
        }
    }
}