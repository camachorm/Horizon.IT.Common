﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace Horizon.IT.Common.Abstractions.Configuration
{
    public class NameValueCollectionIndexedConfiguration : IndexedConfigurationBase
    {
        private readonly NameValueCollection _configurationNameValueCollection;

        public NameValueCollectionIndexedConfiguration(NameValueCollection configurationNameValueCollection)
        {
            _configurationNameValueCollection = configurationNameValueCollection;
        }

        public override IEnumerable<string> Keys => _configurationNameValueCollection.AllKeys;

        public override string this[string key]
        {
            get => _configurationNameValueCollection?[key];
            set => _configurationNameValueCollection[key] = value;
        }
    }
}