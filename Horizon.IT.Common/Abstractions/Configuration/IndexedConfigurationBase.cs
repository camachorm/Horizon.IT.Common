﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Horizon.IT.Common.Abstractions.Interfaces;
using Microsoft.Extensions.Logging;

// ReSharper disable EmptyConstructor
// ReSharper disable UnusedMember.Global

namespace Horizon.IT.Common.Abstractions.Configuration
{
    public abstract class IndexedConfigurationBase : IIndexedConfiguration
    {
        private static int _providerCount;
        private static readonly ReaderWriterLock Lock = new ReaderWriterLock();
        // ReSharper disable InconsistentNaming
        private static readonly MultipleConfiguration _rootConfiguration =
            new MultipleConfiguration(new EnvironmentVariablesIndexedConfiguration());
        // ReSharper restore InconsistentNaming

        private static ILogger _logger;

        protected IndexedConfigurationBase(ILogger logger) : this()
        {
            _logger = logger;
        }

        protected IndexedConfigurationBase()
        {
            ProviderId = $"{GetType().Name}_{++_providerCount}";
            if (_logger == null)
            {
                var lf = new LoggerFactory();
                _logger = lf.CreateLogger(GetType());
            }
            _logger.LogInformation($"Initialized new Configuration Provider: {ProviderId}");
            _logger.LogTrace($"StackTrace at constructor of configuration provider:{System.Environment.NewLine}{new StackTrace()}");
        }

        protected ILogger Logger => _logger;

        /// <summary>
        /// Provides a unique identifier for the provider instance
        /// </summary>
        public string ProviderId { get; }

        public abstract string this[string key] { get; set; }

        public virtual IEnumerable<string> List
        {
            get { return Keys.Select(key => $"{key}/{this[key]}").ToList(); }
        }

        public abstract IEnumerable<string> Keys { get; }

        [ExcludeFromCodeCoverage]
        public virtual IEnumerable<string> ProviderList => new[] { GetType().Name };

        public MultipleConfiguration RootConfiguration
        {
            get
            {
                lock (Lock)
                {
                    return _rootConfiguration;
                }
            }
        }

        public string ReadKey(string key)
        {
            var t = ReadKeyAsync(key);
            t.Wait();
            return t.Result;
        }

        public async Task<string> ReadKeyAsync(string key)
        {
            Logger.LogInformation($"ReadKey called for key: {key}");
            return await Task.Run(() => this[key]);
        }

        public IIndexedConfiguration RegisterProviders(params IIndexedConfiguration[] newProviders)
        {
            lock (Lock)
            {
                foreach (var newProvider in newProviders)
                {
                    _rootConfiguration.RegisterProvider(newProvider);
                }
            }
            return RootConfiguration;
        }

        public static IIndexedConfiguration GetSingleton(ILogger logger = null)
        {
            lock (Lock)
            {
                if (logger != null) _logger = logger;
                return _rootConfiguration;
            }
        }

        public void ResetProviders()
        {
            lock (Lock)
            {
                _rootConfiguration.ConfigurationProviders.Clear();
                _rootConfiguration.RegisterProvider(new EnvironmentVariablesIndexedConfiguration());
            }
        }
    }
}