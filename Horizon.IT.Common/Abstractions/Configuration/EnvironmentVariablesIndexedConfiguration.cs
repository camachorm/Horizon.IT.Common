﻿using System.Collections.Generic;
using System.Linq;

namespace Horizon.IT.Common.Abstractions.Configuration
{
    public class EnvironmentVariablesIndexedConfiguration : IndexedConfigurationBase
    {
        public override string this[string key]
        {
            get => System.Environment.GetEnvironmentVariable(key);
            set => System.Environment.SetEnvironmentVariable(key, value);
        }

        public override IEnumerable<string> Keys => System.Environment.GetEnvironmentVariables().Keys.Cast<string>();
    }
}