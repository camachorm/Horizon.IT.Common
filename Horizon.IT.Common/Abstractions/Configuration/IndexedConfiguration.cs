﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Horizon.IT.Common.Abstractions.Interfaces;

namespace Horizon.IT.Common.Abstractions.Configuration
{
    [ExcludeFromCodeCoverage]
    public class IndexedConfiguration : IndexedConfigurationBase
    {
        private readonly IIndexedConfiguration _indexedConfigurationConfiguration;

        public IndexedConfiguration(IIndexedConfiguration indexedConfiguration)
        {
            _indexedConfigurationConfiguration = indexedConfiguration;
        }

        public override string this[string key]
        {
            get => _indexedConfigurationConfiguration[key];
            set => _indexedConfigurationConfiguration[key] = value;
        }

        public override IEnumerable<string> Keys => _indexedConfigurationConfiguration.Keys;
    }
}