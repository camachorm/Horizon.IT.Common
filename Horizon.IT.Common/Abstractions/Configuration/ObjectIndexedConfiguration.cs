﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;

namespace Horizon.IT.Common.Abstractions.Configuration
{
    public class ObjectIndexedConfiguration<T>: IndexedConfigurationBase where T : class, new()
    {
        protected readonly T Source;

        public ObjectIndexedConfiguration(T source)
        {
            Source = source;
        }

        public ObjectIndexedConfiguration(IOptions<T> source): this(source.Value)
        {
        }

        public override string this[string key]
        {
            get
            {
                var propertyInfo = Source.GetType().GetProperty(key);
                var val = propertyInfo?.GetValue(Source);

                if (propertyInfo?.PropertyType == typeof(string))
                    return val as string;
                return val?.ToString();
            }
            set
            {
                var propertyInfo = Source.GetType().GetProperty(key);
                try
                {
                    propertyInfo?.SetValue(Source, value);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        public override IEnumerable<string> Keys => Source.GetType().GetProperties()
            .Where(p => p.CanRead && p.CanWrite).Select(pi => pi.Name);
    }
}