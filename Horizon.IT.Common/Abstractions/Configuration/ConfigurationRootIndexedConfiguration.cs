﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;

namespace Horizon.IT.Common.Abstractions.Configuration
{
    public class ConfigurationRootIndexedConfiguration : IndexedConfigurationBase
    {
        private readonly IConfiguration _configuration;

        public ConfigurationRootIndexedConfiguration(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public override string this[string key]
        {
            get => _configuration[key];
            set => _configuration[key] = value;
        }

        public override IEnumerable<string> Keys => _configuration.AsEnumerable().Select(kvp => kvp.Key);
    }
}