﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Horizon.IT.Common.Extensions;
using Newtonsoft.Json;

namespace Horizon.IT.Common.ADAL
{
    // We exclude this because there is no possibility of mocking, it requires http connectivity to an actual AD server
    [ExcludeFromCodeCoverage]
    public static class AdalHelper
    {
        
        public static async Task<string> GetTokenAsync(string clientId, string clientSecret, string aadInstance,
            string tenantId, string organizationHostName)
        {
            var loginUrl = aadInstance.AddUriSegment(tenantId).AddUriSegment("oauth2").AddUriSegment("token");
            var resource = organizationHostName;

            var client = new HttpClient();
            var postData =
                $"client_id={clientId}&client_secret={clientSecret}&resource={resource}&grant_type=client_credentials";

            var request = new HttpRequestMessage(HttpMethod.Post, loginUrl)
            {
                Content = new StringContent(postData, Encoding.UTF8)
            };

            request.Content.Headers.Remove("Content-Type");
            request.Content.Headers.TryAddWithoutValidation("Content-Type", "application/x-www-form-urlencoded");

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var responseMessage = await client.SendAsync(request);
            var jsonResponseString = await responseMessage.Content.ReadAsStringAsync();

            var jsonContent = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonResponseString);

            return jsonContent["access_token"];
        }

        public static string GetToken(string clientId, string clientSecret, string aadInstance, string tenantId,
            string organizationHostName)
        {
            var t = GetTokenAsync(clientId, clientSecret, aadInstance, tenantId, organizationHostName);
            t.Wait();
            return t.Result;
        }
    }
}