﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

// ReSharper disable UnusedMember.Global

namespace Horizon.IT.Common.Exceptions
{
    [ExcludeFromCodeCoverage]
    [Serializable]
    public class HorizonException : Exception
    {
        public HorizonException()
        {
        }

        public HorizonException(string message) : base(message)
        {
        }

        public HorizonException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected HorizonException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public ConcurrentDictionary<string, object> AssociatedObjects { get; } = new ConcurrentDictionary<string, object>();
    }
}